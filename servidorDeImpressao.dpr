﻿program servidorDeImpressao;

{$R *.dres}

uses
  Vcl.Forms,
  TratandoErro,
  uAtualizador,
  Controller.ConsumirRest in 'Controller\Controller.ConsumirRest.pas',
  Controller.Impressao in 'Controller\Controller.Impressao.pas',
  Controller.Interfaces in 'Controller\Controller.Interfaces.pas',
  Controller.Log in 'Controller\Controller.Log.pas',
  Controller.Servidor in 'Controller\Controller.Servidor.pas',
  Model.Cliente in 'Model\Model.Cliente.pas',
  Model.Contato in 'Model\Model.Contato.pas',
  Model.Cupom in 'Model\Model.Cupom.pas',
  Model.Endereco in 'Model\Model.Endereco.pas',
  Model.Estabeleciomento in 'Model\Model.Estabeleciomento.pas',
  Model.Extrato in 'Model\Model.Extrato.pas',
  Model.Impressao.Lista in 'Model\Model.Impressao.Lista.pas',
  Model.Impressao in 'Model\Model.Impressao.pas',
  Model.Interfaces in 'Model\Model.Interfaces.pas',
  Model.ItemExtra in 'Model\Model.ItemExtra.pas',
  Model.Itens in 'Model\Model.Itens.pas',
  Model.Pedido in 'Model\Model.Pedido.pas',
  Model.Produto in 'Model\Model.Produto.pas',
  Model.Retirada in 'Model\Model.Retirada.pas',
  Model.Servidor in 'Model\Model.Servidor.pas',
  Model.Txt.Interfaces in 'Model\Txt\Model.Txt.Interfaces.pas',
  Model.Txt.log in 'Model\Txt\Model.Txt.log.pas',
  Model.Txt in 'Model\Txt\Model.Txt.pas',
  Model.PowerCMD.Exceptions in 'Model\PowerShell\Model.PowerCMD.Exceptions.pas',
  Model.PowerCMD.Interfaces in 'Model\PowerShell\Model.PowerCMD.Interfaces.pas',
  Model.PowerCMD in 'Model\PowerShell\Model.PowerCMD.pas',
  Model.ConsumirRest.Interfaces in 'Model\Consumir Rest\Model.ConsumirRest.Interfaces.pas',
  Model.ConsumirRest in 'Model\Consumir Rest\Model.ConsumirRest.pas',
  ServidorImpressao.View.FormHome in 'View\FormHome\ServidorImpressao.View.FormHome.pas' {FormHome},
  Winapi.Windows {FormHome},
  Model.Sat in 'Model\Model.Sat.pas',
  uConsumirRest.RestClient in 'Model\Consumir Rest\uConsumirRest.RestClient.pas',
  uConsumirRest.IndyClient in 'Model\Consumir Rest\uConsumirRest.IndyClient.pas',
  uControleEntregador in 'Model\uControleEntregador.pas';

{$R *.res}

var
   programa, atualizador: THandle;
   titulo: string;
   atualizadorTitulo: string;
begin
  titulo:= 'Servidor de impressão SICLOP';
  atualizadorTitulo:= 'Atualizador SICLOP';
  Tratamento.MostraMensagem:= false;
  programa := FindWindow(nil, PWideChar(titulo));
  atualizador:= FindWindow(nil, PWideChar(atualizadorTitulo));
   if (programa = 0) and (programa = 0) then
   begin
      Application.Initialize;

      ReportMemoryLeaksOnShutdown := (DebugHook<>0);
      Application.MainFormOnTaskbar := true;
      Application.CreateForm(TFormHome, FormHome);
      Application.Run;
   end
   else if (programa = 0) then
      SetForegroundWindow(programa)
   else if (atualizador = 0) then
      SetForegroundWindow(atualizador);

end.
