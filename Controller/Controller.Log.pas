unit Controller.Log;

interface

uses Controller.Interfaces, Model.Txt.log;

type
   TControllerLog = class(TInterfacedObject, iControllerLog)
   private
      Constructor Create;
      procedure deletaLogsAnteriores;
   public
      Destructor Destroy; override;
      class function New: iControllerLog;
      function SalvarLog(arquivo, value: string): iControllerLog;
      function SalvarJsonErro(value: string): iControllerLog;
      function AbrirPasta: iControllerLog;
   end;

implementation

uses
   System.SysUtils;

{ TControllerLog }

function TControllerLog.AbrirPasta: iControllerLog;
begin
   result := self;

   TModelLogTxt.New.AbrirPastaLog;
end;

constructor TControllerLog.Create;
begin

end;

procedure TControllerLog.deletaLogsAnteriores;
begin
   TModelLogTxt.New.deletaLogs;
end;

destructor TControllerLog.Destroy;
begin

   inherited;
end;

class function TControllerLog.New: iControllerLog;
begin
   result := self.Create;
end;

function TControllerLog.SalvarJsonErro(value: string): iControllerLog;
begin
   result := self;
   Randomize;
   TModelLogTxt.New.Gravar(inttostr(Random(1000000)) + '.json', value);
end;

function TControllerLog.SalvarLog(arquivo, value: string): iControllerLog;
begin
   result := self;
   deletaLogsAnteriores;
   TModelLogTxt.New.Gravar(FormatDateTime('ddmm', now)+arquivo + '.log', value);
end;

end.
