unit Controller.Interfaces;

interface

uses
   Vcl.ExtCtrls;

type
   TEvDisplay = procedure(pValue: string) of object;

   iControllerConsumirRest = interface
      ['{F133E119-379A-479E-A93D-68EBA3FBD51C}']
      function FazerRequisicao(codigo: string): string;
      function SetDisplay(value: TEvDisplay): iControllerConsumirRest;
   end;

   iControllerLog = interface
      ['{09AB4688-EC00-452D-9A30-1D9CC35872E9}']
      function SalvarLog(arquivo, value: string): iControllerLog;
      function SalvarJsonErro(value: string): iControllerLog;
      function AbrirPasta: iControllerLog;
   end;

   iControllerImpressao = interface
      ['{FA86F8F3-251C-47F3-94A6-1C605140A5D0}']
      function SetDisplay(value: TEvDisplay): iControllerImpressao;
      function GerarImprimessao(JSON: string): iControllerImpressao;
      function Imprimir: Boolean;
   end;

   iControllerServidor = interface
      ['{83B0EEC1-3DC5-4DDC-BAD6-4DF80C9616DA}']
      function GetCodigo: string;
      function GetConectado: Boolean;
      function SetLogDisplay(value: TEvDisplay): iControllerServidor;
      function SetServidorDeImpressaoDisplay(value: TEvDisplay): iControllerServidor;
      function CodigoServido: iControllerServidor;
      function LiberarAcesso(login, senha: string): Boolean;
      function Conectar(aPergunta: boolean = true): iControllerServidor;
      function IniciarWindows: iControllerServidor;
      function SolicitarImpressaoREST(tmr: TTimer): iControllerServidor;
      function SolicitarImpressaoPasta(tmr: TTimer; var tem: Boolean)
        : iControllerServidor;
      property conectado: Boolean read GetConectado;
   end;

implementation

end.
