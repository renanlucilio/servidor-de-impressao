unit Controller.Servidor;

interface

uses Controller.Interfaces, Vcl.ExtCtrls, Model.Interfaces;

type
   TControllerServidor = class(TInterfacedObject, iControllerServidor)
   private
      Fservidor: iModelServidor;
      FLog: TEvDisplay;
      FCodigoServido: TEvDisplay;
      FConectado: Boolean;
      FCodigo: string;
      class var Finstancia: iControllerServidor;
      Constructor Create;
      function VerificarRetornoJson(aJSON: string): Boolean;
   public
      Destructor Destroy; override;
      class function New: iControllerServidor;
      function GetCodigo: string;
      function GetConectado: Boolean;
      function SetLogDisplay(value: TEvDisplay): iControllerServidor;
      function SetServidorDeImpressaoDisplay(value: TEvDisplay): iControllerServidor;
      function CodigoServido: iControllerServidor;
      function LiberarAcesso(login, senha: string): Boolean;
      function Conectar(aPergunta: boolean = true): iControllerServidor;
      function IniciarWindows: iControllerServidor;
      function SolicitarImpressaoREST(tmr: TTimer): iControllerServidor;
      function SolicitarImpressaoPasta(tmr: TTimer; var tem: Boolean)
        : iControllerServidor;
      property conectado: Boolean read GetConectado;
   end;

implementation

uses
   System.SysUtils, Model.Servidor, System.Threading, Controller.ConsumirRest,
   Controller.Impressao, Controller.Log, System.Classes, System.IOUtils,
   System.DateUtils;

{ TControllerServidor }

function TControllerServidor.CodigoServido: iControllerServidor;
begin
   Result := self;
   FCodigo := TModelServidor.New.gerarCodigoServidor.codigoServidor;
   FCodigoServido(FCodigo);
end;

function TControllerServidor.Conectar(aPergunta: boolean = true): iControllerServidor;
begin
   Result := self;

   FConectado := Fservidor.ligarServidor(aPergunta).conectado;

   if FConectado then
      FLog(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
        ' Servidor Conectado')
   else
      FLog(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
        ' Servidor Desconectado');
end;

constructor TControllerServidor.Create;
begin
   inherited;
   FConectado := false;
   Fservidor := TModelServidor.New;
end;

destructor TControllerServidor.Destroy;
begin
   inherited;
end;

function TControllerServidor.GetCodigo: string;
begin
   Result := FCodigo;
end;

function TControllerServidor.GetConectado: Boolean;
begin
   Result := FConectado;
end;

function TControllerServidor.IniciarWindows: iControllerServidor;
begin
   Result := self;
   Fservidor.iniciaComWindows;
end;

function TControllerServidor.LiberarAcesso(login, senha: string): Boolean;
begin
   FLog(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
     ' fazendo verifica��o do login');

   Result := Fservidor.login(login, senha).logado;

   if Result then
      FLog(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) + ' T�cnico logado')
   else
      FLog(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
        ' erro ao fazer login')
end;

class function TControllerServidor.New: iControllerServidor;
begin
   if Assigned(Finstancia) then
      Result := Finstancia
   else
      Result := self.Create();
end;

function TControllerServidor.SetLogDisplay(value: TEvDisplay): iControllerServidor;
begin
   Result := self;
   FLog := value;
end;

function TControllerServidor.SetServidorDeImpressaoDisplay(
  value: TEvDisplay): iControllerServidor;
begin
  Result := self;
  FCodigoServido := value;
end;

function TControllerServidor.SolicitarImpressaoPasta(tmr: TTimer;
  var tem: Boolean): iControllerServidor;
var
   arquivoJson: TSearchRec;
   JSON: TStringList;
   pasta: string;
   qtdArquivos: Integer;
   Impressao: iControllerImpressao;
   dataArquivo: TDateTime;
   Task: ITask;
begin

   tmr.Enabled := false;

   JSON := TStringList.Create;

   pasta := tpath.GetDocumentsPath + '\SICLOP\ServidorImpressao\';

   Impressao := TControllerImpressao.New();

   Impressao.SetDisplay(FLog);

   qtdArquivos := FindFirst(pasta + '*.json', faArchive, arquivoJson);

   tem := not(qtdArquivos = 0);

   Task := TTask.Create(
      procedure
      begin
         try
            while qtdArquivos = 0 do
            begin
               JSON.LoadFromFile(pasta + arquivoJson.Name);
               DeleteFile(pasta + arquivoJson.Name);

               FileAge(pasta + arquivoJson.Name, dataArquivo);

               if DayOf(dataArquivo) = DayOf(Now) then
               begin
                  if not JSON.Text.IsEmpty then
                  begin

                     Impressao.GerarImprimessao(JSON.Text).Imprimir;
                  end;
               end;

               qtdArquivos := FindNext(arquivoJson);
            end;
         finally
            tmr.Enabled := FConectado;
            FreeAndNil(JSON);
         end;
      end);
   Task.Start;

end;

function TControllerServidor.SolicitarImpressaoREST(tmr: TTimer)
  : iControllerServidor;
var
   JSON: string;
   Task: ITask;
   FRequisicao: iControllerConsumirRest;
   FImpressao: iControllerImpressao;
begin
   FLog(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
     ' verificando se h� impress�es no servidor');
   FRequisicao := TControllerConsumirRest.New;
   FRequisicao.SetDisplay(FLog);
   FImpressao := TControllerImpressao.New();
   FImpressao.SetDisplay(FLog);
   tmr.Enabled := false;

   Task := TTask.Create(
      procedure()
      begin
         try
            JSON := FRequisicao.FazerRequisicao(FCodigo);
            if not JSON.IsEmpty then
            begin
               try
                if VerificarRetornoJson(JSON) then
                  FImpressao.GerarImprimessao(JSON).Imprimir;
               except
                  TControllerLog.New.SalvarJsonErro(JSON);
               end;
            end;
         finally
            tmr.Enabled := FConectado;
         end;

      end);

   Task.Start;
end;

function TControllerServidor.VerificarRetornoJson(aJSON: string): Boolean;
begin
  if aJSON.Contains('Error 001') then
  begin
    FLog(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) + ' **ERRO**Chave inv�lida');
    Exit(false);
  end;

  if aJSON.Contains('Error 002') then
  begin
    FLog(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) + '**ERRO**Informa��es n�o encontradas');
    Conectar(false);
    Exit(false);
  end;

  if aJSON.Contains('Error 003') then
  begin
    FLog(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) + '**ERRO**Servidor n�o informou chave de acesso');
    Exit(false);
  end;

  if aJSON.Contains('Failed opening required') then
  begin
    FLog(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) + '**ERRO**Pizza-Web est� em atualiza��o');
    Exit(false);
  end;
  result:= true;
end;

end.
