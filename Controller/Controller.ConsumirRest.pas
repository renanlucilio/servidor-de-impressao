unit Controller.ConsumirRest;

interface

uses Controller.Interfaces;

type
   TControllerConsumirRest = class(TInterfacedObject, iControllerConsumirRest)
   private
      FDisplay: TEvDisplay;
      Constructor Create;
   public
      Destructor Destroy; override;
      class function New: iControllerConsumirRest;
      function FazerRequisicao(codigo: string): string;
      function SetDisplay(value: TEvDisplay): iControllerConsumirRest;
   end;

implementation

uses
   Model.ConsumirRest.Interfaces, System.SysUtils, Model.ConsumirRest;

{ TControllerConsumirRest }

constructor TControllerConsumirRest.Create;
begin

end;

destructor TControllerConsumirRest.Destroy;
begin

   inherited;
end;

function TControllerConsumirRest.FazerRequisicao(codigo: string): string;
var
   ConsumirRest: IConsumirRest;
begin
   ConsumirRest := TModelConsumirRest.New;
   ConsumirRest.SetCodigo(codigo);
   Result := ConsumirRest.GetJSON;
end;

class function TControllerConsumirRest.New: iControllerConsumirRest;
begin
   Result := self.Create;
end;

function TControllerConsumirRest.SetDisplay(value: TEvDisplay)
  : iControllerConsumirRest;
begin
   Result := self;
   FDisplay := value;
end;

end.
