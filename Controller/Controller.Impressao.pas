unit Controller.Impressao;

interface

uses Controller.Interfaces, Model.Interfaces, ACBrUtil, Model.Impressao,
   Controller.Log, Model.Impressao.Lista, Vcl.Printers;

type
   TControllerImpressao = class(TInterfacedObject, iControllerImpressao)
   private
      FlistaImpressoes: iModelListaImpressoes;
      Fdisplay: TEvDisplay;
      Constructor Create;
      function PreparaImpressora(impressora, computador: string): Boolean;
   public
      Destructor Destroy; override;
      class function New: iControllerImpressao;
      function SetDisplay(value: TEvDisplay): iControllerImpressao;
      function GerarImprimessao(JSON: string): iControllerImpressao;
      function Imprimir: Boolean;
   end;

implementation

uses
   System.SysUtils, System.Classes, Model.PowerCMD, REST.JSON;

{ TControllerImpressao }

constructor TControllerImpressao.Create;
begin

end;

destructor TControllerImpressao.Destroy;
begin

   inherited;
end;

function TControllerImpressao.GerarImprimessao(JSON: string)
  : iControllerImpressao;
begin
   Result := Self;

   try
      if JSON <> '' then
      begin
         try
            FlistaImpressoes := TJson.JsonToObject<TModelListaImpressoes>(JSON)
         except
          Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) + JSON);
         end;
      end;
   except
      raise Exception.Create('N�o foi poss�vel converter os dados');
   end;
end;

function TControllerImpressao.Imprimir: Boolean;
var
   impressoes: TModelImpressoes;
   Impressao: TModelImpressao;
   JSON: string;
begin
   Result := False;
   try
      if Assigned(FlistaImpressoes) then
      begin
         if Length(FlistaImpressoes.ListaImpressoes) > 0 then
         begin
            Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) + ' H� ' +
              Length(FlistaImpressoes.ListaImpressoes).ToString +
              ' Pedidos na lista');
            for impressoes in FlistaImpressoes.ListaImpressoes do
            begin
               for Impressao in impressoes.Impressao do
               begin
                  if Assigned(Impressao.Sat) then
                  begin
                    Impressao.Sat.SalvaTXT;
                    Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) + 'CUPOM SAT Baixado');
                  end;

                  if not Impressao.nomeImpressora.IsEmpty() and
                     not Impressao.nomeComputador.IsEmpty then
                  begin
                    try
                      if PreparaImpressora(Impressao.NomeImpressora,
                        Impressao.NomeComputador) then
                      begin
                         Impressao.Imprimir;
                         Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) + 'Imprimindo');
                      end
                      else
                      begin
                        if (Printer.PrinterIndex >= 0) then
                        begin
                          Impressao.NomeImpressora := Printer.Printers[Printer.PrinterIndex];
                        end
                        else
                          raise Exception.Create('n�o h� impressora padr�o');

                        Impressao.Imprimir;
                        Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) + 'Imprimindo na impressora padr�o');
                      end;
                    except
                       JSON := TJson.ObjectToJsonString
                           (TModelImpressao(Impressao));
                         TControllerLog.New.SalvarJsonErro(JSON);
                         Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
                           ' N�o foi poss�vel enviar arquivo para impress�o');
                    end;
                  end;

               end;
            end;
         end;
      end;

   except
      Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
        ' Houve um erro ao imprimir');
      raise Exception.Create('Erro ao gerar impress�es');
   end;
end;

class function TControllerImpressao.New: iControllerImpressao;
begin
   Result := Self.Create;
end;

function TControllerImpressao.PreparaImpressora(impressora,
  computador: string): Boolean;
const
   cListaImpressoras =
     'Get-WmiObject -Class Win32_Printer | ft Name, SystemName, Status ';
var
   listaAux: TStringList;
   item, linha: string;
   resultado: string;
begin
  try
    try
      Result := False;

      listaAux := TStringList.Create;

      TModelPowerCMD.New(HInstance).ExecComando(cListaImpressoras, resultado);
      AddDelimitedTextToList(resultado, #13, listaAux);
      if listaAux.Count > 0 then
      begin
        for item in listaAux do
        begin
          impressora:= LowerCase(impressora);
          computador:= LowerCase(computador);
          linha:= LowerCase(item);
           if (linha.Contains(impressora)) and
             (linha.Contains(computador)) then
           begin

              if linha.Contains('error') then
              begin
                 Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] = ', Now) +
                   'Impressora com erro');
                 Result := False;
              end
              else
              begin
                 Result := True;
                 exit;
              end;
           end
           else
              Result := False;
        end;
      end
      else
        raise Exception.Create('Erro');
    finally
      FreeAndNil(listaAux);
    end;
  except
    Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
                   'N�o � poss�vel verificar as impressoras e computadores instalados.');
    Fdisplay(FormatDateTime('[hh:nn dd/mm/yyyy] =', Now) +
               'Windows ou anti-virus est� bloqueando a verifica��o.');
    Result := False;
  end;

end;

function TControllerImpressao.SetDisplay(value: TEvDisplay)
  : iControllerImpressao;
begin
   Result := Self;
   Fdisplay := value;
end;

end.
