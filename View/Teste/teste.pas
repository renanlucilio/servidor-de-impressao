unit teste;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes,
   Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
   Vcl.ExtCtrls,
   ServidorImpressao.Controller.Interfaces, ServidorImpressao.Model.Extrato,
   Rest.Json;

type
   TForm1 = class(TForm)
      Btn1: TButton;
      Mmo1: TMemo;
      tmr1: TTimer;
      Button1: TButton;
      procedure Btn1Click(Sender: TObject);
      procedure FormShow(Sender: TObject);
      procedure Button1Click(Sender: TObject);
   private
      procedure MostraMemo(pValue: string);
      { Private declarations }
   public
      { Public declarations }
   end;

var
   Form1: TForm1;
   Servidor: iControllerServidor;

implementation

uses
   ServidorImpressao.Controller.Servidor;

{$R *.dfm}

procedure TForm1.Btn1Click(Sender: TObject);
begin

   if Servidor.Conectado then
      Servidor.SolicitarImpressaoREST(tmr1);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
   colunas: TModelExtrato;
begin
   colunas := TModelExtrato.Create;
   colunas.titulo := 'Renan lucilio';
   Mmo1.Lines.Add(TJson.ObjectToJsonString(colunas));
end;

procedure TForm1.FormShow(Sender: TObject);
begin
   Servidor := TControllerServidor.New(Application.Handle);
   Servidor.SetDisplay(MostraMemo);
   Servidor.CodigoServido;
   Servidor.Conectar;
end;

procedure TForm1.MostraMemo(pValue: string);
begin
   Mmo1.Lines.Add(pValue);
end;

end.
