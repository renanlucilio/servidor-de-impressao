﻿unit ServidorImpressao.View.FormHome;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes,
   Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls,
   Vcl.Buttons,
   Vcl.StdCtrls, Vcl.Menus, Controller.Interfaces, Controller.Servidor,
   System.ImageList, Vcl.ImgList;

type
   TFormHome = class(TForm)
      PnlInferior: TPanel;
      BtnServidor: TBitBtn;
      PnlPrincipal: TPanel;
      MmoRegistros: TMemo;
      PnlSuperior: TPanel;
      BtnPastaLog: TSpeedButton;
      BtnCodigo: TSpeedButton;
      BtnLogar: TBitBtn;
      EdtLogin: TEdit;
      EdtSenha: TEdit;
      PPMenu: TPopupMenu;
      A1: TMenuItem;
      N1: TMenuItem;
      S1: TMenuItem;
      TmrOpcoes: TTimer;
      TmrRequisicoes: TTimer;
    lblVersao: TLabel;
    pnl1: TPanel;
    ImgList: TImageList;
    imgStatus: TImage;
    lblStatus: TLabel;
    TmrLayout: TTimer;
      procedure TrayIconDblClick(Sender: TObject);
      procedure BtnLogarClick(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure BtnCodigoClick(Sender: TObject);
      procedure BtnPastaLogClick(Sender: TObject);
      procedure TmrOpcoesTimer(Sender: TObject);
      procedure TmrRequisicoesTimer(Sender: TObject);
      procedure FormShow(Sender: TObject);
    procedure BtnServidorClick(Sender: TObject);
    procedure TmrLayoutTimer(Sender: TObject);
   private
      Servidor: iControllerServidor;
      procedure MostraNoMemo(pvalue: string);
      procedure MostraMsg(pvalue: string);
      { Private declarations }

      procedure LigarDesativar();
   public
      { Public declarations }
   end;

var
   FormHome: TFormHome;
   VerificaPasta: Boolean;

implementation

uses
   Controller.Log,
   Model.LibUtil;

{$R *.dfm}

procedure TFormHome.BtnCodigoClick(Sender: TObject);
begin
   Servidor.CodigoServido;
end;

procedure TFormHome.BtnLogarClick(Sender: TObject);
var
   vValidado: Boolean;
begin
   vValidado := Servidor.LiberarAcesso(UpperCase(EdtLogin.Text),
     UpperCase(EdtSenha.Text));

   if vValidado then
   begin
    if Servidor.conectado then
      LigarDesativar;

    TmrOpcoes.Enabled := true;
    EdtLogin.Visible := false;
    EdtSenha.Visible := false;
    BtnLogar.Visible := false;
    BtnCodigo.Visible := true;
    BtnPastaLog.Visible := true;
   end;

end;

procedure TFormHome.BtnPastaLogClick(Sender: TObject);
begin
   TControllerLog.New.AbrirPasta;
end;

procedure TFormHome.BtnServidorClick(Sender: TObject);
begin
  LigarDesativar;
end;

procedure TFormHome.FormDestroy(Sender: TObject);
begin
  LigarDesativar;
   TControllerLog.New.SalvarLog('Log_Sistema', MmoRegistros.Text);
   Servidor := nil;
end;

procedure TFormHome.FormShow(Sender: TObject);
begin
   lblVersao.Caption:= GetVersaoPrograma(paramstr(0));
   Servidor := TControllerServidor.New();
   Servidor.SetLogDisplay(MostraNoMemo);
   Servidor.SetServidorDeImpressaoDisplay(MostraNoMemo);
   Servidor.CodigoServido;
   Servidor.SetServidorDeImpressaoDisplay(MostraMsg);
   Servidor.IniciarWindows;
   MmoRegistros.Clear;
   LigarDesativar;
end;

procedure TFormHome.LigarDesativar;
begin
  TmrRequisicoes.Enabled := Servidor.Conectar.Conectado;
end;

procedure TFormHome.MostraMsg(pvalue: string);
begin
   InputBox('Código do servidor', 'Esse é o código de validação: ', pvalue)
end;

procedure TFormHome.MostraNoMemo(pvalue: string);
begin
   MmoRegistros.Lines.Add(pvalue);
end;

procedure TFormHome.TmrLayoutTimer(Sender: TObject);
begin
  if Servidor.Conectado then
  begin
    if lblStatus.caption <> 'Online' then
    begin
      lblStatus.caption:= 'Online';
      ImgList.GetBitmap(1,imgStatus.Picture.Bitmap);
      imgStatus.Refresh();
      BtnServidor.Caption := 'Desativar Servidor de Impressão';
    end;
  end
  else
  begin
    if lblStatus.caption <> 'Offline' then
    begin
      lblStatus.caption:= 'Offline';
      ImgList.GetBitmap(0,imgStatus.Picture.Bitmap);
      imgStatus.Refresh();
      BtnServidor.Caption := 'Iniciar Servidor de Impressão';
    end;
  end;
end;

procedure TFormHome.TmrOpcoesTimer(Sender: TObject);
begin
   EdtLogin.Visible := True;
   EdtSenha.Visible := True;
   BtnLogar.Visible := True;
   BtnCodigo.Visible := False;
   BtnPastaLog.Visible := False;
   EdtLogin.Clear;
   EdtSenha.Clear;
   TmrOpcoes.Enabled := False;
end;

procedure TFormHome.TmrRequisicoesTimer(Sender: TObject);
begin
  if temInternet then
  begin
   if not(VerificaPasta) then
   begin
      Servidor.SolicitarImpressaoPasta(TmrRequisicoes, VerificaPasta);
   end
   else
      Servidor.SolicitarImpressaoREST(TmrRequisicoes);
  end
  else
    LigarDesativar;
end;

procedure TFormHome.TrayIconDblClick(Sender: TObject);
begin
   Self.Show;
end;

end.
