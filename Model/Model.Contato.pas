unit Model.Contato;

interface

type
   TModelContato = class
   private
      Ftipo: string;
      Fcontato: string;
      Fwhatsapp: string;
   public
      constructor Create;
      destructor Destroy; override;
      property tipo: string read Ftipo write Ftipo;
      property Contato: string read Fcontato write Fcontato;
      property whatsapp: string read Fwhatsapp write Fwhatsapp;
   end;

implementation

{ TModelContato }

constructor TModelContato.Create;
begin
   inherited Create;
end;

destructor TModelContato.Destroy;
begin

   inherited;
end;

end.
