unit uControleEntregador;

interface

uses
  uRelatorio.Interfaces,
  Model.Cliente,
  Model.Pedido,
  Model.Estabeleciomento;

type
  TControleEntregador = class
  private
    Frelatorio:           IRelatorio;
    Fcliente:             TModelCliente;
    FPedido:              TModelPedido;
    FEstabelecimento:     TModelEstabelecimento;
    FEntregador:          string;
    FHoraSaidaEntregador: string;
    FHoraBaixaPedido: string;
    procedure CriarBlocoTitulo;
    procedure CriarBlocoEstabelecimento;
    procedure CriarBlocoPedido;
    procedure CriarBlocoEntrega;
    procedure PrepararImpressao(impressora, computador, usuario: string);
  public
    destructor Destroy; override;
    property Cliente:             TModelCliente         read Fcliente             write Fcliente;
    property Pedido:              TModelPedido          read FPedido              write FPedido;
    property Estabelecimento:     TModelEstabelecimento read FEstabelecimento     write FEstabelecimento;
    property Entregador:          string                read FEntregador          write FEntregador;
    property horaBaixaPedido:     string                read FHoraBaixaPedido     write FHoraBaixaPedido;
    property horaSaidaEntregador: string                read FHoraSaidaEntregador write FHoraSaidaEntregador;

    procedure Imprimir(nomeComputador, nomeImpressora, usuario: string; margemEsquerda: integer); overload;
    procedure imprimir(nomeComputador, nomeImpressora, usuario: string); overload;
  end;

implementation

uses
  uRelatorio.InformacacaoImportante,
  uRelatorio.InformacaoRodape,
  uRelatorio.InformativosSimples,
  uRelatorio.Matricial,
  uRelatorio.Termica,
  System.Generics.Collections,
  System.SysUtils,
  Model.Contato;

{ TControleEntregador }

procedure TControleEntregador.Imprimir(nomeComputador, nomeImpressora,
  usuario: string);
begin
  Frelatorio := TRelatorioMatricial.Create();

  Frelatorio.Ref;

  PrepararImpressao(nomeImpressora, nomeComputador, usuario);

   Frelatorio.imprimir(
    'Cupom de retirada: ' + Pedido.numPedido,
    nomeComputador,
    nomeImpressora);
end;

procedure TControleEntregador.CriarBlocoEstabelecimento;
var
   titulo: string;
   informacoes: TList<string>;
   novaLinha: TStringBuilder;
   item: TModelContato;
begin
   informacoes := TList<string>.Create;
   novaLinha := TStringBuilder.Create;
   try
      if Assigned(estabelecimento) then
      begin
         titulo := estabelecimento.nomeFantasia;

         if Assigned(estabelecimento.endereco) then
         begin
            novaLinha.Append('Endere�o: ');

            if not estabelecimento.endereco.logradouro.IsEmpty then
            begin
               novaLinha.Append(estabelecimento.endereco.logradouro);

               if not estabelecimento.endereco.numero.IsEmpty then
                  novaLinha.Append(', ')
                    .Append(estabelecimento.endereco.numero);

               if not estabelecimento.endereco.complemento.IsEmpty then
                  novaLinha.Append(' - ')
                    .Append(estabelecimento.endereco.complemento);

               if not estabelecimento.endereco.bairro.IsEmpty then
                  novaLinha.Append(' - ')
                    .Append(estabelecimento.endereco.bairro);

               if not estabelecimento.endereco.cidade.IsEmpty then
                  novaLinha.Append(' - ')
                    .Append(estabelecimento.endereco.cidade);

               if not estabelecimento.endereco.uf.IsEmpty then
                  novaLinha.Append(' - ').Append(estabelecimento.endereco.uf);

               if not estabelecimento.endereco.cep.IsEmpty then
                  novaLinha.Append(' - ').Append(estabelecimento.endereco.cep);

               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;

            if not estabelecimento.endereco.complementoRua.IsEmpty then
            begin
               novaLinha.Append('Complemento Rua: ')
                 .Append(estabelecimento.endereco.complementoRua);
               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;

            if not estabelecimento.endereco.pontoReferencia.IsEmpty then
            begin
               novaLinha.Append('Ponto de referencia: ')
                 .Append(estabelecimento.endereco.pontoReferencia);
               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;

         end;

         if Assigned(estabelecimento.contatos) then
         begin
            for item in estabelecimento.contatos do
            begin
               if not item.tipo.IsEmpty then
                  novaLinha.Append(item.tipo).Append(': ');

               if not item.Contato.IsEmpty then
                  novaLinha.Append(item.Contato);

               if CharInSet(item.whatsapp[low(item.whatsapp)], ['s', 'S']) then
                  novaLinha.Append(' *WHATSAPP');

               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;
         end;

      end;

      Frelatorio.addInformacaoImportante(TInformacaoImportante.New(titulo,
        12, 52, 7, informacoes));

   finally

      novaLinha.Free;
      informacoes.Free;
   end;
end;

procedure TControleEntregador.PrepararImpressao(impressora, computador,
  usuario: string);
var
  infRodaPe: TList<string>;
  linha: TStringBuilder;
begin

  infRodaPe := TList<string>.Create;
  linha:= TStringBuilder.Create();
  try
    CriarBlocoEstabelecimento;
    CriarBlocoTitulo;
    CriarBlocoPedido;
    CriarBlocoEntrega;

    if not computador.IsEmpty then
      linha.append('\\'+ computador);

    if not impressora.IsEmpty then
      linha.append('\'+ impressora);

    if not computador.IsEmpty then
      linha.append('  Usu�rio: '+ usuario);

    infRodaPe.add(linha.ToString());

    infRodaPe.add('www.centralsiclop.com.br');
    Frelatorio.addInformacaoRodape(TInformacaoRodape.New(infRodaPe, 7, 84));
  finally
    linha.Free;
    infRodaPe.Free;
  end;
end;

procedure TControleEntregador.CriarBlocoEntrega;
var
  titulo: string;
  informacoes: TList<string>;
  novaLinha: TStringBuilder;

begin
  informacoes:= TList<string>.Create();
  novaLinha:= TStringBuilder.Create();
  try

    titulo:= 'Informa��es de entrega';

    if not HoraSaidaEntregador.IsEmpty then
    begin
      novaLinha.Clear;
      novaLinha.Append('Hora de sa�da do Entregador:').Append(horaSaidaEntregador);
      informacoes.Add(novaLinha.ToString());
    end;

    if not HoraBaixaPedido.IsEmpty then
    begin
      novaLinha.Clear;
      novaLinha.Append('Hora da baixa do pedido:').Append(horaBaixaPedido);
      informacoes.Add(novaLinha.ToString());
    end;

    if Assigned(Cliente) then
    begin
      if not Cliente.Endereco.taxaEntrega.IsEmpty then
      begin
        novaLinha.Clear;
        novaLinha.Append('Taxa de entrega: R$').Append(Cliente.Endereco.taxaEntrega);
        informacoes.Add(novaLinha.ToString());
      end;
    end;

    Frelatorio.addInformacaoImportante(TInformacaoImportante.New(titulo,
        12, 52, 7, informacoes));

  finally
    informacoes.Free;
    novaLinha.Free;
  end;

end;

procedure TControleEntregador.CriarBlocoPedido;
var
   titulo: string;
   informacoes: TList<string>;
   novaLinha: TStringBuilder;
   item: TModelContato;
begin
  informacoes := TList<string>.Create;
  novaLinha := TStringBuilder.Create;
  try
    if Assigned(Pedido) and Assigned(Cliente) then
    begin
      titulo := 'Informa��es Pedido';

      if not Entregador.IsEmpty then
      begin
        novaLinha.Clear;
        novaLinha.Append('Entregador: ').Append(Entregador);
        informacoes.Add(novaLinha.ToString());
      end;

      if not Pedido.numPedido.IsEmpty then
      begin
        novaLinha.Clear;
        novaLinha.Append('Pedido: ').Append(Pedido.numPedido);
        informacoes.Add(novaLinha.ToString());
      end;

      if not Pedido.dataHora.IsEmpty then
      begin
        novaLinha.Clear;
        novaLinha.Append('Data/Hora: ').Append(Pedido.dataHora);
        informacoes.Add(novaLinha.ToString());
      end;

      if not Cliente.nome.IsEmpty then
      begin
        novaLinha.Clear;
        novaLinha.Append('Cliente: ').Append(Cliente.nome);
        informacoes.Add(novaLinha.ToString());
      end;

      if Assigned(Cliente.Contato) then
      begin
        novaLinha.Clear;
        for item in Cliente.Contato do
        begin
          if not item.tipo.IsEmpty then
            novaLinha.Append(item.tipo).Append(': ');

          if not item.Contato.IsEmpty then
            novaLinha.Append(item.Contato);

          if CharInSet(item.whatsapp[low(item.whatsapp)], ['s', 'S']) then
            novaLinha.Append(' *WHATSAPP');

          informacoes.add(novaLinha.ToString);
          novaLinha.Clear;
        end;
      end;

      if not Cliente.endereco.logradouro.IsEmpty then
      begin
         novaLinha.Append(Cliente.endereco.logradouro);

         if not Cliente.endereco.numero.IsEmpty then
            novaLinha.Append(', ')
              .Append(Cliente.endereco.numero);

         if not Cliente.endereco.complemento.IsEmpty then
            novaLinha.Append(' - ')
              .Append(Cliente.endereco.complemento);

         if not Cliente.endereco.bairro.IsEmpty then
            novaLinha.Append(' - ')
              .Append(Cliente.endereco.bairro);

         if not Cliente.endereco.cidade.IsEmpty then
            novaLinha.Append(' - ')
              .Append(Cliente.endereco.cidade);

         if not Cliente.endereco.uf.IsEmpty then
            novaLinha.Append(' - ').Append(Cliente.endereco.uf);

         if not Cliente.endereco.cep.IsEmpty then
            novaLinha.Append(' - ').Append(Cliente.endereco.cep);

         informacoes.add(novaLinha.ToString);
         novaLinha.Clear;
      end;

      Frelatorio.addInformacaoImportante(TInformacaoImportante.New(titulo,
        12, 52, 7, informacoes));
    end;
  finally
  novaLinha.Free;
  informacoes.Free;
  end;
end;

procedure TControleEntregador.CriarBlocoTitulo;
var
  informacoes: TList<string>;
begin
  informacoes:= TList<string>.Create;
  try
    Frelatorio.addInformacaoImportante(TInformacaoImportante.New('Controle de Entregador',
          12, 52, 6, informacoes));
  finally
    informacoes.Free;
  end;
end;

destructor TControleEntregador.Destroy;
begin
  if Assigned(Cliente) then
    Fcliente.Free;

  if Assigned(Estabelecimento) then
    FEstabelecimento.Free;

  if Assigned(Pedido) then
    FPedido.Free;

  inherited;
end;

procedure TControleEntregador.Imprimir(nomeComputador, nomeImpressora,
  usuario: string; margemEsquerda: integer);
begin
  Frelatorio:= TRelatorioTermica.create(margemEsquerda);

  Frelatorio.Ref;

  PrepararImpressao(nomeImpressora, nomeComputador, usuario);

   Frelatorio.imprimir(
    'Cupom de retirada: ' + Pedido.numPedido,
    nomeComputador,
    nomeImpressora);
end;

end.
