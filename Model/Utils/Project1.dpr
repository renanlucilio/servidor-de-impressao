program Project1;

uses
  Vcl.Forms,
  Unit1 in '..\..\..\..\teste\Unit1.pas' {Form1},
  Model.Cliente in '..\Model.Cliente.pas',
  Model.Contato in '..\Model.Contato.pas',
  Model.Cupom in '..\Model.Cupom.pas',
  Model.Endereco in '..\Model.Endereco.pas',
  Model.Estabeleciomento in '..\Model.Estabeleciomento.pas',
  Model.Extrato in '..\Model.Extrato.pas',
  Model.Impressao in '..\Model.Impressao.pas',
  Model.Interfaces in '..\Model.Interfaces.pas',
  Model.ItemExtra in '..\Model.ItemExtra.pas',
  Model.Itens in '..\Model.Itens.pas',
  Model.Pedido in '..\Model.Pedido.pas',
  Model.Produto in '..\Model.Produto.pas',
  Model.Retirada in '..\Model.Retirada.pas',
  Model.Servidor in '..\Model.Servidor.pas',
  Model.ConsumirRest.Interfaces in '..\Consumir Rest\Model.ConsumirRest.Interfaces.pas',
  Model.ConsumirRest in '..\Consumir Rest\Model.ConsumirRest.pas',
  Model.PowerCMD.Exceptions in '..\PowerShell\Model.PowerCMD.Exceptions.pas',
  Model.PowerCMD.Interfaces in '..\PowerShell\Model.PowerCMD.Interfaces.pas',
  Model.PowerCMD in '..\PowerShell\Model.PowerCMD.pas',
  Model.Relatorio.InformacacaoImportante in '..\Relatorio\Model.Relatorio.InformacacaoImportante.pas',
  Model.Relatorio.InformacaoLista in '..\Relatorio\Model.Relatorio.InformacaoLista.pas',
  Model.Relatorio.InformacaoRodape in '..\Relatorio\Model.Relatorio.InformacaoRodape.pas',
  Model.Relatorio.InformativosSimples in '..\Relatorio\Model.Relatorio.InformativosSimples.pas',
  Model.Relatorio.Interfaces in '..\Relatorio\Model.Relatorio.Interfaces.pas',
  Model.Relatorio in '..\Relatorio\Model.Relatorio.pas',
  Model.Txt.Interfaces in '..\Txt\Model.Txt.Interfaces.pas',
  Model.Txt.log in '..\Txt\Model.Txt.log.pas',
  Model.Txt in '..\Txt\Model.Txt.pas',
  Model.LibUtil in 'Model.LibUtil.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
