unit Model.Pedido;

interface

type
   TModelPedido = class
   private
      FvalorParcial: string;
      FvalorTotal: string;
      FnumPedido: string;
      FdataHora: string;
      FvalorDesconto: string;
      FvalorAcrescimo: string;
      FnumMesa: string;
      Fgarcom: string;
      Ftroco: string;
      FdividirPor: string;
      FvalorDivisao: string;
      FtaxaServico: string;
      FobsPagamento: string;
      Fbandeira: string;
      FaltPedido: string;
   public
      constructor Create;
      destructor Destroy; override;
      //
      property valorParcial: string read FvalorParcial write FvalorParcial;
      property valorTotal: string read FvalorTotal write FvalorTotal;
      property numPedido: string read FnumPedido write FnumPedido;
      property dataHora: string read FdataHora write FdataHora;
      property valorDesconto: string read FvalorDesconto write FvalorDesconto;
      property valorAcrescimo: string read FvalorAcrescimo
        write FvalorAcrescimo;
      property numMesa: string read FnumMesa write FnumMesa;
      property garcom: string read Fgarcom write Fgarcom;
      property troco: string read Ftroco write Ftroco;
      property dividirPor: string read FdividirPor write FdividirPor;
      property valorDivisao: string read FvalorDivisao write FvalorDivisao;
      property taxaServico: string read FtaxaServico write FtaxaServico;
      property obsPagamento: string read FobsPagamento write FobsPagamento;
      property bandeira: string read Fbandeira write Fbandeira;
      property altPedido: string read FaltPedido write FaltPedido;
   end;

implementation

{ TModelPedido }

constructor TModelPedido.Create;
begin
   inherited Create;
end;

destructor TModelPedido.Destroy;
begin

   inherited;
end;

end.
