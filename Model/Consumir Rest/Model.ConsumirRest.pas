﻿unit Model.ConsumirRest;

interface

uses
  Model.ConsumirRest.Interfaces;

type
  TModelConsumirRest = class(TInterfacedObject, IConsumirRest)
  private
    FURL: string;
    FURI: string;
    FCodigoServidor: string;
    class procedure geraArquivoConfiguracao;
  public
    constructor Create;
    destructor Destroy; override;
    class function New: IConsumirRest;

    function SetCodigo(codigo: string): IConsumirRest;
    function GetJSON: string;

    property URL: string read FURL write FURL;
  end;

implementation

uses
  System.SysUtils,
  Vcl.Forms,
  JclSysInfo,
  PersistenciaConfiguracao.Interfaces,
  PersistenciaConfiguracao.Ini,
  Factory.PersistenciaConfigucao,
  uConsumirRest.RestClient,
  uConsumirRest.IndyClient;

{ TModelConsumirRest }

constructor TModelConsumirRest.Create;
var
  Ini: iSalvaCarregaConfiguracao;
begin
  inherited Create;
  Ini := TPersistenciaIni.New;
  FURI := '/wbl_json_impressoes/?chaveCliente={CODIGO}&random={RANDOM}';

  Ini.carrega(self);
  FURL := Ini.getResultado().Items['URL'];
end;

destructor TModelConsumirRest.Destroy;
begin
  
  inherited;
end;

class procedure TModelConsumirRest.geraArquivoConfiguracao;
var
  inst: TModelConsumirRest;
  Ini: iSalvaCarregaConfiguracao;
begin
  inst:= TModelConsumirRest.Create();
  Ini := TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  try
    if not fileexists(ExtractFilePath(ParamStr(0)) + '\config.ini') then
    begin
      Ini.salva(inst);
    end;
  finally
    inst.Free;
  end;
end;

function TModelConsumirRest.GetJSON: string;
var
  vValida: Boolean;
  vRandom: string;
  request: IRequestRest;
  windows: string;
begin
  vValida := (FURL <> '') and (FURI <> '');
  if vValida then
  begin
    windows:=  GetWindowsVersionstring();

    if windows = 'Windows 10' then
      request:= TRestCliente.Create()
    else
      request:= TIndyClient.Create();

    Randomize;
    vRandom := IntToStr(Random(1000000));

    result:= request
              .SetUrl(FURL)
              .SetUri(FURI)
                .SetParams('CODIGO', FCodigoServidor)
                .SetParams('RANDOM', vRandom)
                .GetResponse();
  end
  else
  begin
    application.Terminate;
    raise Exception.Create('Falta preencher a URL e o URI do serviço');
  end;

end;

class function TModelConsumirRest.New: IConsumirRest;
begin
  Result := self.Create;
end;

function TModelConsumirRest.SetCodigo(codigo: string): IConsumirRest;
begin
  FCodigoServidor := codigo;
end;

initialization

TModelConsumirRest.geraArquivoConfiguracao();

end.
