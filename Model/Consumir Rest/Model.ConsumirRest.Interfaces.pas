unit Model.ConsumirRest.Interfaces;

interface

type
   IConsumirRest = interface
      ['{83B7CB14-8BA6-4DB2-9239-A133B7F2D0A7}']
      function SetCodigo(pValue: string): IConsumirRest;
      function GetJSON: string;
   end;

   IRequestRest = interface
    ['{93A8F4F2-D3EA-4008-9295-11E24A152B15}']
    function SetUrl(url: string): IRequestRest;
    function SetUri(uri: string): IRequestRest;
    function SetParams(key, value: string): IRequestRest;
    function GetResponse(): string;
    function Ref(): IRequestRest;
   end;

implementation

end.
