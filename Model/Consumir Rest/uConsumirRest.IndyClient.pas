unit uConsumirRest.IndyClient;

interface

uses
  Model.ConsumirRest.Interfaces,
  IdBaseComponent,
  IdComponent,
  IdTCPConnection,
  IdTCPClient,
  IdHTTP,
  IdSSLOpenSSL,
  System.SysUtils;

type
  TIndyClient = class(TInterfacedObject, IRequestRest)
  private
    FClient:TIdHTTP;
    FRota: TStringBuilder;
  public
    constructor Create();
    destructor Destroy();override;
    function Ref(): IRequestRest;

    function SetUrl(url: string): IRequestRest;
    function SetUri(uri: string): IRequestRest;
    function SetParams(key, value: string): IRequestRest;
    function GetResponse(): string;
  end;

implementation

{ TIndyClient }

constructor TIndyClient.Create;
var
  ssl:TIdSSLIOHandlerSocketOpenSSL;
begin
  FClient:= TIdHTTP.Create(nil);
  ssl:= TIdSSLIOHandlerSocketOpenSSL.Create(FClient);
  ssl.SSLOptions.Method:= sslvTLSv1_2;
  ssl.SSLOptions.SSLVersions:= [sslvTLSv1_2];

  FClient.IOHandler:= ssl;
  FClient.HandleRedirects:= True;
  FRota:= TStringBuilder.Create();
end;

destructor TIndyClient.Destroy;
begin
  FClient.Free;
  FRota.Free;
  inherited;
end;

function TIndyClient.GetResponse: string;
var
  rota: string;
begin
  rota:= FRota.ToString();
  Result:= FClient.Get(rota);
end;

function TIndyClient.Ref: IRequestRest;
begin
  result:= Self;
end;

function TIndyClient.SetParams(key, value: string): IRequestRest;
begin
  result:= self;
  FRota.Replace(Concat('{', key, '}'), value);
end;

function TIndyClient.SetUri(uri: string): IRequestRest;
begin
  result:= Self;
  FRota.Append(uri);
end;

function TIndyClient.SetUrl(url: string): IRequestRest;
begin
  result:= Self;
  FRota.Append(url);
end;

end.
