unit uConsumirRest.RestClient;

interface

uses
  Model.ConsumirRest.Interfaces,
  IPPeerClient, 
  REST.Client,
  REST.Types;

type
  TRestCliente = class(TInterfacedObject, IRequestRest)
  private
    FRequest: TRESTRequest;
    FClient: TRESTClient;
  public
    constructor Create(typeRequest: TRESTRequestMethod = rmGET);
    destructor Destroy; override;
    function Ref(): IRequestRest;

    function SetUrl(url: string): IRequestRest;
    function SetUri(uri: string): IRequestRest;
    function SetParams(key, value: string): IRequestRest;
    function GetResponse(): string;
  end;


implementation

{ TRestCliente }

constructor TRestCliente.Create(typeRequest: TRESTRequestMethod = rmGET);
begin
  FClient := TRESTClient.Create(nil);
  FRequest := TRESTRequest.Create(nil);
  FClient.ResetToDefaults;
  FRequest.ResetToDefaults;

  FClient.AcceptCharset := 'utf-8, iso-8859-1;q=0.5';
  FClient.AcceptEncoding:= 'br;q=1.0, gzip;q=0.8, *;q=0.1';
  FClient.Accept        := 'application/json, text/plain, text/html, application/xml; q=0.9,*/*;q=0.8';

  FRequest.Method:= typeRequest;

  FRequest.Client := FClient;
end;

destructor TRestCliente.Destroy;
begin
  FClient.Free;
  FRequest.Free;
  inherited;
end;

function TRestCliente.GetResponse: string;
begin
  try
    FRequest.Execute();  
    FRequest.Response.ContentType:= 'application/json';
    Result:= FRequest.Response.Content;
  except
    Result:= FRequest.Response.StatusText;
  end;
end;

function TRestCliente.Ref: IRequestRest;
begin
  result:= self;
end;

function TRestCliente.SetParams(key, value: string): IRequestRest;
begin
  result:= self;
  FRequest.Params.AddUrlSegment(key, value);
end;

function TRestCliente.SetUri(uri: string): IRequestRest;
begin
  result:= self;
  FRequest.Resource:= uri;
end;

function TRestCliente.SetUrl(url: string): IRequestRest;
begin
  result:= self;
  FClient.BaseURL:= url;
end;

end.
