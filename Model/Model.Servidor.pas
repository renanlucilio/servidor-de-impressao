unit Model.Servidor;

interface

uses Model.Interfaces, Model.LibUtil, Winapi.Windows;

type
   TModelServidor = class(TInterfacedObject, iModelServidor)
   private
      FcodigoServidor: string;
      FConectado: boolean;
      Flogin: string;
      Fsenha: string;
      Flogado: boolean;
      function GetcodigoServidor: string;
      function getLogado: boolean;
      function getConectado: boolean;
   public
      Destructor Destroy; override;
      Constructor Create;
      class function New: iModelServidor;
      function gerarCodigoServidor: iModelServidor;
      function login(login, senha: string): iModelServidor;
      function ligarServidor(aPerguntar: Boolean = true): iModelServidor;
      function iniciaComWindows: iModelServidor;
      property codigoServidor: string read GetcodigoServidor;
      property logado: boolean read getLogado;
      property conectado: boolean read getConectado;
   end;

implementation

uses
   System.SysUtils, JclSysInfo, ACBrUtil, Vcl.Forms, System.UITypes;

{ TModelServidor }

constructor TModelServidor.Create;
begin
   inherited;
   Fsenha := 'WEB787878';
   Flogin := 'SICLOP';
end;

destructor TModelServidor.Destroy;
begin

   inherited;
end;

function TModelServidor.gerarCodigoServidor: iModelServidor;
var
   aux: string;
   codigo: TStringBuilder;
   I: Integer;
begin
   result := self;
   codigo := TStringBuilder.Create;
   try
      aux := GetVolumeSerialNumber('C');
      codigo.Append(Copy(aux, 0, length(aux)));
      aux := GetLocalComputerName;
      codigo.Append(Copy(aux, 0, length(aux)));
      aux := GetOSVersionString;
      codigo.Append(Copy(aux, length(aux) - 2, length(aux)));
      aux := inttostr(GetWindowsBuildNumber);
      codigo.Append(Copy(aux, 0, length(aux)));
      aux := NtProductTypeString;
      codigo.Append(Copy(aux, 0, length(aux)));
      codigo.Replace(' ', '');
      aux := TiraAcentos(codigo.ToString);
      aux := TiraPontos(aux);

      for I := 0 to length(aux) do
         FcodigoServidor := FcodigoServidor + inttostr(ord(aux[I]));

   finally
      FreeAndNil(codigo);
   end;
end;

function TModelServidor.GetcodigoServidor: string;
begin
   result := FcodigoServidor;
end;

function TModelServidor.getConectado: boolean;
begin
   result := FConectado;
end;

function TModelServidor.getLogado: boolean;
begin
   result := Flogado;
end;

function TModelServidor.iniciaComWindows: iModelServidor;
begin
   result := self;
   Model.LibUtil.iniciaComWindows('Servidor de impress�o');
end;

function TModelServidor.ligarServidor(aPerguntar: Boolean): iModelServidor;
begin
   result := self;
   if FConectado  then
   begin
    if not temInternet() then
      FConectado:= false;

//    if aPerguntar and
//         (Application.MessageBox('Deseja desativar o servidor de impress�o?', 'Desativar?', MB_ICONQUESTION+MB_YESNO)= mrYes) then
//      FConectado := false
//    else
      FConectado := false;

   end
   else
   begin
     if temInternet then
      FConectado:= true;
   end;
end;

function TModelServidor.login(login, senha: string): iModelServidor;
begin
   result := self;
   Flogado := (Flogin = login) and (Fsenha = senha);
end;

class function TModelServidor.New: iModelServidor;
begin
   result := self.Create;
end;

end.
