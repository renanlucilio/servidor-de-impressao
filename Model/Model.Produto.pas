unit Model.Produto;

interface

uses Model.ItemExtra;

type
   TModelProduto = class
   private
      Fdescricao: string;
      FitemExtra: TArray<TModelItemExtra>;
   public
      constructor Create;
      destructor Destroy; override;
      //
      property descricao: string read Fdescricao write Fdescricao;
      property ItemExtra: TArray<TModelItemExtra> read FitemExtra
        write FitemExtra;
   end;

implementation

{ TModelProduto }

constructor TModelProduto.Create;
begin
   inherited Create;
end;

destructor TModelProduto.Destroy;
var
   I: Integer;
begin
   if length(FitemExtra) > 0 then
      for I := 0 to length(FitemExtra) - 1 do
         FitemExtra[I].Free;

   inherited;
end;

end.
