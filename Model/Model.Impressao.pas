unit Model.Impressao;

interface

uses
  System.SysUtils,
  Model.Retirada,
  Model.Extrato,
  Model.Cupom,
  Model.Sat,
  uControleEntregador;

type
  TModelImpressao = class
  private
    FlarguraBobina: string;
    FmEsquerda: string;
    FmDireita: string;
    FmFundo: string;
    FmTopo: string;
    FnomeImpressora: string;
    FnomeComputador: string;
    FimpCod: string;
    FimpValor: string;
    Fdesmenbrada: string;
    FnumCopia: integer;
    FtipoCupom: string;
    FsegundaVia: string;
    FfechamentoMesa: string;
    Fgenerica: string;
    Fcupom: TModelCupom;
    FreciboRetirada: TModelRetirada;
    Fextrato: TArray<TModelExtrato>;
    Fsat: TInformacoesSAT;
    Foperador: string;
    FcontroleEntregador: TControleEntregador;
    procedure PrepararCupom;
  public
    Constructor Create;
    Destructor Destroy; override;
    property larguraBobina: string read FlarguraBobina write FlarguraBobina;
    property mEsquerda: string read FmEsquerda write FmEsquerda;
    property mDireita: string read FmDireita write FmDireita;
    property mFundo: string read FmFundo write FmFundo;
    property mTopo: string read FmTopo write FmTopo;
    property nomeImpressora: string read FnomeImpressora write FnomeImpressora;
    property nomeComputador: string read FnomeComputador write FnomeComputador;
    property Operador: string read Foperador write Foperador;
    property impCod: string read FimpCod write FimpCod;
    property impValor: string read FimpValor write FimpValor;
    property desmenbrada: string read Fdesmenbrada write Fdesmenbrada;
    property numCopia: integer read FnumCopia write FnumCopia;
    property tipoCupom: string read FtipoCupom write FtipoCupom;
    property segundaVia: string read FsegundaVia write FsegundaVia;
    property fechamentoMesa: string read FfechamentoMesa write FfechamentoMesa;
    property generica: string read Fgenerica write Fgenerica;
    property Cupom: TModelCupom read Fcupom write Fcupom;
    property reciboRetirada: TModelRetirada read FreciboRetirada
      write FreciboRetirada;
    property Extrato: TArray<TModelExtrato> read Fextrato write Fextrato;
    property sat: TInformacoesSAT read Fsat write Fsat;
    property controleEntregador: TControleEntregador read FcontroleEntregador write FcontroleEntregador;
    procedure imprimir;
  end;

  TarImpressoes = TArray<TModelImpressao>;

  TModelImpressoes = class
  private
    FImpressao: TarImpressoes;
  public
    destructor Destroy; override;
    property Impressao: TarImpressoes read FImpressao write FImpressao;
  end;

implementation


{ TModelImpressao }

constructor TModelImpressao.Create;
begin
  inherited;
end;

destructor TModelImpressao.Destroy;
var
  i: integer;
begin

  if assigned(FreciboRetirada) then
    FreciboRetirada.Free;

  if length(Fextrato) > 0 then
    for i := 0 to length(Fextrato) - 1 do
      Fextrato[i].Free;

  if assigned(Fcupom) then
    Fcupom.Free;

  if Assigned(Fsat) then
    Fsat.Free;

  if Assigned(controleEntregador) then
    FcontroleEntregador.Free;

  inherited;
end;

procedure TModelImpressao.imprimir;
begin
    if CharInSet(generica[low(generica)], ['s', 'S']) then
    begin
      if Assigned(reciboRetirada) then
        reciboRetirada.imprimir(nomeComputador, nomeImpressora, Operador);

      if Assigned(Cupom) then
      begin
        PrepararCupom;
        Cupom.imprimir(nomeComputador, nomeImpressora, Operador);
      end;

      if assigned(Extrato) and (length(Extrato) > 0) then
      begin
        Extrato[0].Imprimir(nomeComputador, nomeImpressora, Operador, Extrato);
      end;

      if Assigned(controleEntregador) then
      begin
        controleEntregador.Imprimir(nomeComputador, nomeImpressora, Operador);
      end;
    end
    else
    begin
      if Assigned(reciboRetirada) then
        reciboRetirada.imprimir(nomeComputador, nomeImpressora, Operador,
          StrToIntDef(mEsquerda, 0));

      if Assigned(Cupom) then
      begin
        PrepararCupom;
        Cupom.imprimir(nomeComputador, nomeImpressora, Operador,
          StrToIntDef(mEsquerda, 0));
      end;

      if assigned(Extrato) and (length(Extrato) > 0) then
      begin
        Extrato[0].Imprimir(nomeComputador, nomeImpressora, Operador,
          StrToIntDef(mEsquerda, 0), Extrato);
      end;


      if Assigned(controleEntregador) then
      begin
        controleEntregador.Imprimir(nomeComputador, nomeImpressora, Operador,
          StrToIntDef(mEsquerda, 0));
      end;
    end;


end;

procedure TModelImpressao.PrepararCupom;
begin
  if Trim(tipoCupom).IsEmpty then
    tipoCupom := '1';
  if Trim(segundaVia).IsEmpty then
    segundaVia := 'n';
  if Trim(impCod).IsEmpty then
    impCod := 'n';
  if Trim(impValor).IsEmpty then
    impValor := 'n';
  if Trim(fechamentoMesa).IsEmpty then
    fechamentoMesa := 'n';
  Cupom.preparaImpressao(StrToIntDef(tipoCupom, 1), CharInSet(segundaVia[low(segundaVia)], ['s', 'S']), CharInSet(impCod[low(impCod)], ['s', 'S']), CharInSet(impValor[low(impValor)], ['s', 'S']), CharInSet(fechamentoMesa[low(fechamentoMesa)], ['s', 'S']));
end;{ TModelImpressoes }

destructor TModelImpressoes.Destroy;
var
  i: integer;
begin
  if length(FImpressao) > 0 then
    for i := 0 to length(FImpressao) - 1 do
      FImpressao[i].Free;
  inherited;
end;

end.
