unit Model.Endereco;

interface

type
   TModelEndereco = class
   private
      FtaxaEntrega: string;
      Flogradouro: string;
      Fnumero: string;
      Fcomplemento: string;
      FpontoReferencia: string;
      Fbairro: string;
      Fcidade: string;
      Fuf: string;
      Fcep: string;
      FcomplementoRua: string;
   public
      constructor Create;
      destructor Destroy; override;
      //
      property taxaEntrega: string read FtaxaEntrega write FtaxaEntrega;
      property logradouro: string read Flogradouro write Flogradouro;
      property numero: string read Fnumero write Fnumero;
      property complemento: string read Fcomplemento write Fcomplemento;
      property pontoReferencia: string read FpontoReferencia
        write FpontoReferencia;
      property bairro: string read Fbairro write Fbairro;
      property cidade: string read Fcidade write Fcidade;
      property uf: string read Fuf write Fuf;
      property cep: string read Fcep write Fcep;
      property complementoRua: string read FcomplementoRua
        write FcomplementoRua;
   end;

implementation

{ TModelEndereco }

constructor TModelEndereco.Create;
begin
   inherited Create;
end;

destructor TModelEndereco.Destroy;
begin

   inherited;
end;

end.
