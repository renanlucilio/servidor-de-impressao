unit Model.Extrato;

interface

uses
  uRelatorio.Interfaces,
  System.Generics.Collections;

type
  TLinhas = TArray<string>;

  TModelExtrato = class
  private
    Frelatorio: iRelatorio;
    Ftitulo: string;
    FqtdCaracteres: TArray<integer>;
    Fcolunas: TArray<TLinhas>;
    procedure dadosExtratos(extratos: TArray<TModelExtrato>);
    procedure PrepararImpressao(extratos: TArray<TModelExtrato>;impressora, computador, usuario: string);
  public
    Constructor Create;
    Destructor Destroy; override;
    property titulo: string read Ftitulo write Ftitulo;
    property qtdCaracteres: TArray<integer> read FqtdCaracteres
      write FqtdCaracteres;
    property colunas: TArray<TLinhas> read Fcolunas write Fcolunas;

    procedure Imprimir(nomeComputador, nomeImpressora, usuario: string; margemEsquerda: Integer;extratos: TArray<TModelExtrato>); overload;
    procedure Imprimir(nomeComputador, nomeImpressora, usuario: string;extratos: TArray<TModelExtrato>); overload;
  end;

implementation

{ TModelExtrato }

uses
  uRelatorio.InformacacaoImportante,
  uRelatorio.InformacaoLista,
  uRelatorio.InformacaoRodape,
  uRelatorio.InformativosSimples,
  System.SysUtils,
  uRelatorio.Matricial, uRelatorio.Termica;

constructor TModelExtrato.Create;
begin
  inherited;
end;

procedure TModelExtrato.dadosExtratos(extratos: TArray<TModelExtrato>);
var
  ListString: TList<string>;
  ListInteger: TList<integer>;
  num: integer;
  linha: TLinhas;
  item: string;
  Extrato: TModelExtrato;
begin
  ListString := TList<string>.Create;
  ListInteger := TList<integer>.Create;

  try
    for Extrato in extratos do
    begin
      ListString.clear;
      Frelatorio.addInformacaoImportante
        (TInformacaoImportante.New('--' + Extrato.titulo, 12, 60, 9,
        ListString));

      ListInteger.clear;
      for num in Extrato.FqtdCaracteres do
      begin
        ListInteger.Add(num);
      end;

      for linha in Extrato.colunas do
      begin
        ListString.clear;
        for item in linha do
        begin
          if item = '-' then
          begin
            Frelatorio.addInformacaoSimples
              (TInformacaoSimples.New
              ('----------------------------------------', '', 14, 100));
          end
          else
            ListString.Add(item);
        end;

        if ListString.Count > 0 then
          Frelatorio.addInformacoesLista(TInformacaoLista.New(ListString, ListInteger));

      end;



    end;
  finally
    FreeAndNil(ListInteger);
    FreeAndNil(ListString);
  end;
end;

destructor TModelExtrato.Destroy;
begin

  inherited;
end;

procedure TModelExtrato.Imprimir(nomeComputador, nomeImpressora, usuario: string; margemEsquerda: Integer;extratos: TArray<TModelExtrato>);
begin
  Frelatorio:= TRelatorioTermica.create(margemEsquerda);

  Frelatorio.ref;

  PrepararImpressao(extratos, nomeImpressora, nomeComputador, usuario);

  Frelatorio.imprimir(
    'Extrato do dia: ' + dateTostr(now()),
     nomeComputador,
     nomeImpressora);
end;

procedure TModelExtrato.imprimir(nomeComputador, nomeImpressora, usuario: string;extratos: TArray<TModelExtrato>);
begin
  Frelatorio := TRelatorioMatricial.Create();

  Frelatorio.ref;

  PrepararImpressao(extratos, nomeImpressora, nomeComputador, usuario);

  Frelatorio.imprimir(
    'Extrato do dia: ' + dateTostr(now()),
     nomeComputador,
     nomeImpressora);
end;

procedure TModelExtrato.PrepararImpressao(extratos: TArray<TModelExtrato>;impressora, computador, usuario: string);
var
  infRodaPe: TList<string>;
  linha: TStringBuilder;
begin

  infRodaPe := TList<string>.Create;
  linha:= TStringBuilder.Create();
  try
    dadosExtratos(extratos);


    if not computador.IsEmpty then
      linha.append('\\'+ computador);

    if not impressora.IsEmpty then
      linha.append('\'+ impressora);

    if not computador.IsEmpty then
      linha.append('  Usu�rio: '+ usuario);

    infRodaPe.add(linha.ToString());

    infRodaPe.Add('www.centralsiclop.com.br');
    Frelatorio.addInformacaoRodape(TInformacaoRodape.New(infRodaPe, 7, 70));
  finally
    linha.Free;
    infRodaPe.Free;
  end;
end;


end.
