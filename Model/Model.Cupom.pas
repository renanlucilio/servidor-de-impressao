unit Model.Cupom;

interface

uses
  uRelatorio.Interfaces,
  Model.Cliente,
  Model.Itens,
  Model.Estabeleciomento,
  Model.Pedido,
  Model.Contato,
  Model.Produto,
  Model.ItemExtra,
  System.Classes,
  uRelatorio.Termica;

type
  TModelCupom = class
  private
    Frelatorio: IRelatorio;
    FtipoPedido: integer;
    FsegundaVia: boolean;
    FimpCod: boolean;
    FImpValor: boolean;
    FfechamentoMesa: boolean;
    Fcliente: TModelCliente;
    FAltPedido: Boolean;
    Fitens: TArray<TModelItens>;
    Festabelecimento: TModelEstabelecimento;
    Fpedido: TModelPedido;
    FGenerica: Boolean;
    procedure dadosEstabelecimento;
    procedure dadosEntrega;
    procedure dadosPedido;
    procedure FormataPedidoEntrega;
    procedure FormataPedidoBalcao;
    procedure FormataPedidoMesa;
    procedure FormataPedidoCartao;
    procedure dadosLegProduto;
    procedure dadosProduto;
    procedure subTotal;
    procedure descontos;
    procedure acrescimos;
    procedure taxaEntrega;
    procedure total;
    procedure troco;
    procedure dividirPor;
    procedure bandeiraCartao;
    procedure obsPagamento;
    procedure dadosCliente;
    procedure PrepararImpressao(impressora, computador, usuario: string);
  public
    constructor Create;
    destructor Destroy; override;
    property Pedido: TModelPedido read Fpedido write Fpedido;
    property Cliente: TModelCliente read Fcliente write Fcliente;
    property Itens: TArray<TModelItens> read Fitens write Fitens;
    property estabelecimento: TModelEstabelecimento read Festabelecimento
      write Festabelecimento;
    procedure preparaImpressao(tipoCupom: integer; segundaVia, impCod, impValor,
      fechamentoMesa: boolean);
    procedure Imprimir(nomeComputador, nomeImpressora, usuario: string; margemEsquerda: integer); overload;
    procedure imprimir(nomeComputador, nomeImpressora, usuario: string); overload;
  end;

implementation

uses
  uRelatorio.InformacaoRodape,
  System.SysUtils,
  uRelatorio.InformacacaoImportante,
  uRelatorio.InformativosSimples,
  uRelatorio.InformacaoLista,
  System.Generics.Collections,
  uRelatorio.Matricial;

{ TModelCupom }

procedure TModelCupom.acrescimos;
begin
  if assigned(Pedido) then
    if (Pedido.ValorAcrescimo <> '0,00') and not(Pedido.ValorAcrescimo.isEmpty)
    then
    begin
      Frelatorio.addInformacaoSimples(TInformacaoSimples.New('Acrescimo:',
        'R$' + Pedido.ValorAcrescimo, 9, 28));
    end;
end;

procedure TModelCupom.bandeiraCartao;
begin
  if assigned(Pedido) then
    if not(Pedido.bandeira.isEmpty) then
    begin
      Frelatorio.addInformacaoSimples
        (TInformacaoSimples.New('Cart�o: '+Pedido.bandeira, '', 9, 28));
    end;
end;

constructor TModelCupom.Create;
begin
  fgenerica:= false;
end;

procedure TModelCupom.dadosCliente;
var
  informacoes: TList<string>;
  novaLinha: TStringBuilder;
  item: TModelContato;
begin
  informacoes := TList<string>.Create;
  novaLinha := TStringBuilder.Create;
  try
    if assigned(Cliente) then
    begin

      if not Cliente.Nome.isEmpty then
      begin
        novaLinha
            .Append('Cliente: ').Append(Cliente.Nome);

        informacoes.add(novaLinha.ToString());
        novaLinha.clear();
      end;

      if not Cliente.ultimoPedido.isEmpty then
      begin
        if Cliente.ultimoPedido = 'NOVO CLIENTE' then
          novaLinha
            .Append('**NOVO CLIENTE**')
        else
          novaLinha
              .Append('�ltimo Pedido: ').Append(Cliente.ultimoPedido);

        informacoes.add(novaLinha.ToString());
        novaLinha.clear();
      end;


      if not Cliente.Restricao.isEmpty then
      begin
        novaLinha
            .Append('Restri��o: ').Append(Cliente.Restricao);

        informacoes.add(novaLinha.ToString());
        novaLinha.clear();
      end;

      if assigned(Cliente.Contato) then
      begin
            novaLinha
              .Append('Contatos: ');

        for item in Cliente.Contato do
        begin
          if not item.Contato.isEmpty then
            novaLinha
              .Append(item.Contato);

          if CharInSet(item.whatsapp[low(item.whatsapp)], ['s', 'S']) then
            novaLinha
              .Append('*WHATSAPP');

           novalinha
            .append(' ');
        end;

        informacoes.add(novaLinha.ToString);
      end;

      Frelatorio
        .addInformacaoImportante(TInformacaoImportante
          .New('',10, 50, 9, informacoes));
    end;
  finally
    informacoes.free;
    novaLinha.free;
  end;
end;

procedure TModelCupom.dadosEntrega;
var
  informacoes: TStringList;
  novaLinha: TStringBuilder;

begin
  informacoes := TStringList.Create;
  novaLinha := TStringBuilder.Create;
  try
    if assigned(Cliente) then
    begin

      if assigned(Cliente.Endereco) then
      begin
        if not Cliente.Endereco.logradouro.isEmpty then
        begin

          novaLinha
            .Append('Endere�o: ').Append(Cliente.Endereco.logradouro);

          if not Cliente.Endereco.numero.isEmpty then
            novaLinha
              .Append(', ').Append(Cliente.Endereco.numero);

          if not Cliente.Endereco.complemento.isEmpty then
            novaLinha.Append(' - ').Append(Cliente.Endereco.complemento);

          informacoes.add(novaLinha.ToString);

          Frelatorio
            .AddInformacaoSimples(TInformacaoSimples
              .New(informacoes.text,'', 9, 35));

//          Frelatorio
//          .addInformacaoSimples(TInformacaoSimples
//           .New('*ALTERA��O','', 9, 50));
          novaLinha.Clear;
          informacoes.Clear;
         end;

        if not Cliente.Endereco.bairro.isEmpty then
        begin
          novaLinha
            .Append('Bairro: ').Append(Cliente.Endereco.bairro);

          informacoes.add(novaLinha.ToString);

          novaLinha.Clear;
        end;

        if not Cliente.Endereco.cidade.isEmpty then
        begin

          novaLinha
            .Append('Cidade: ').Append(Cliente.Endereco.cidade);

          informacoes.add(novaLinha.ToString);

          novaLinha.Clear;
        end;

        if not Cliente.Endereco.cep.isEmpty then
        begin
          novaLinha
            .Append('CEP: ').Append(Cliente.Endereco.cep);

          informacoes.add(novaLinha.ToString);

          novaLinha.Clear;
        end;

        if not Cliente.Endereco.complementoRua.isEmpty then
        begin
          novaLinha
            .Append('Complemento Rua: ').Append(Cliente.Endereco.complementoRua);

          informacoes.add(novaLinha.ToString);

          novaLinha.Clear;
        end;

        if not Cliente.Endereco.pontoReferencia.isEmpty then
        begin
          novaLinha
            .Append('Ponto de referencia: ').Append(Cliente.Endereco.pontoReferencia);

          informacoes.add(novaLinha.ToString);

          novaLinha.Clear;
        end;
      end;
    end;

      Frelatorio
            .AddInformacaoSimples(TInformacaoSimples
              .New(informacoes.text,'', 9, 35));

  finally
    informacoes.free;
    novaLinha.free;
  end;

end;

procedure TModelCupom.dadosEstabelecimento;
var
  titulo: string;
  informacoes: TList<string>;
  novaLinha: TStringBuilder;
  item: TModelContato;
begin
  informacoes := TList<string>.Create;
  novaLinha := TStringBuilder.Create;
  try
    if assigned(estabelecimento) then
    begin
      titulo := estabelecimento.nomeFantasia;

      if not estabelecimento.cnpj.isEmpty then
        informacoes.add('CNPJ/CPF: ' + estabelecimento.cnpj);

      if assigned(estabelecimento.Endereco) then
      begin
        novaLinha.Append('Endere�o: ');

        if not estabelecimento.Endereco.logradouro.isEmpty then
        begin
          novaLinha.Append(estabelecimento.Endereco.logradouro);

          if not estabelecimento.Endereco.numero.isEmpty then
            novaLinha.Append(', ').Append(estabelecimento.Endereco.numero);

          if not estabelecimento.Endereco.complemento.isEmpty then
            novaLinha.Append(' - ')
              .Append(estabelecimento.Endereco.complemento);

          if not estabelecimento.Endereco.bairro.isEmpty then
            novaLinha.Append(' - ').Append(estabelecimento.Endereco.bairro);

          if not estabelecimento.Endereco.cidade.isEmpty then
            novaLinha.Append(' - ').Append(estabelecimento.Endereco.cidade);

          if not estabelecimento.Endereco.uf.isEmpty then
            novaLinha.Append(' - ').Append(estabelecimento.Endereco.uf);

          if not estabelecimento.Endereco.cep.isEmpty then
            novaLinha.Append(' - ').Append(estabelecimento.Endereco.cep);

          informacoes.add(novaLinha.ToString);
          novaLinha.Clear;
        end;

        if not estabelecimento.Endereco.complementoRua.isEmpty then
        begin
          novaLinha.Append('Complemento Rua: ')
            .Append(estabelecimento.Endereco.complementoRua);
          informacoes.add(novaLinha.ToString);
          novaLinha.Clear;
        end;

        if not estabelecimento.Endereco.pontoReferencia.isEmpty then
        begin
          novaLinha.Append('Ponto de referencia: ')
            .Append(estabelecimento.Endereco.pontoReferencia);
          informacoes.add(novaLinha.ToString);
          novaLinha.Clear;
        end;

      end;

      if assigned(estabelecimento.contatos) then
      begin
         novaLinha.Append('Contatos: ');

        for item in estabelecimento.contatos do
        begin

          if not item.Contato.isEmpty then
            novaLinha.Append(item.Contato);

          if CharInSet(item.whatsapp[low(item.whatsapp)], ['s', 'S']) then
            novaLinha.Append('*WHATSAPP');

          novaLinha.Append(' ');
        end;

        informacoes.add(novaLinha.ToString);
      end;

    end;

    Frelatorio.addInformacaoImportante(TInformacaoImportante.New(titulo,
      12, 40, 7, informacoes));
  finally
    novaLinha.free;
    informacoes.free;
  end;
end;

procedure TModelCupom.dadosLegProduto;
var
  legenda: TList<string>;
  qtdChar: TList<integer>;
  aux: integer;
  charsCod: Integer;
  charsDescricao: Integer;
  charsQtd: Integer;
  charsValor: Integer;
begin
  legenda := TList<string>.Create;
  qtdChar := TList<integer>.Create;

    if FGenerica then
  begin
    charsCod:=3;
    charsDescricao:=28;
    charsQtd:=6;
    charsValor:=5;
  end
  else
  begin
    charsCod:=3;
    charsDescricao:=13;
    charsQtd:=6;
    charsValor:=5;
  end;
  try
    aux := 0;
    if length(Itens) > 0 then
    begin
      Frelatorio.addInformacaoImportante
        (TInformacaoImportante.New('PRODUTOS', 10, 35, 0, legenda));

      if FimpCod then
      begin
        legenda.add('COD');
        qtdChar.add(charsCod);
      end
      else
        aux := charsCod;

      if not FImpValor then
        aux := aux + charsValor;

      legenda.add('Descri��o');
      qtdChar.add(aux + charsDescricao);

      legenda.add('QTD');
      qtdChar.add(charsQtd);

      if FImpValor then
      begin
        legenda.add('Valor');
        qtdChar.add(charsValor);
      end;

      Frelatorio.addInformacoesLista(TInformacaoLista.New(legenda,
        qtdChar));
    end;

  finally
    legenda.free;
    qtdChar.free;
  end;

end;

procedure TModelCupom.dadosPedido;
var
  titulo: string;
  informacoes: TList<string>;
  novaLinha: TStringBuilder;
begin
  informacoes := TList<string>.Create;
  novaLinha := TStringBuilder.Create;
  try
    if assigned(Pedido) then
    begin

      if FsegundaVia then
      begin
        Frelatorio
          .addInformacaoSimples(TInformacaoSimples
           .New('*SEGUNDA VIA','', 9, 50));
      end;

      if FAltPedido then
      begin
        Frelatorio
          .addInformacaoSimples(TInformacaoSimples
           .New('*ALTERA��O','', 9, 50));
      end;

      case FtipoPedido of
        1:
          begin
            titulo := ('*PEDIDO ENTREGA');
            novaLinha.Append('Pedido: ').Append(Pedido.numPedido);
          end;
        2:
          begin
            titulo := ('*PEDIDO BALC�O');
            novaLinha.Append('Pedido: ').Append(Pedido.numPedido);
          end;
        3:
          begin
             titulo := ('*PEDIDO MESA');
            novaLinha.Append('Mesa: ').Append(Pedido.numMesa);
          end;
        4:
          begin
            titulo := ('*PEDIDO CART�O');
            novaLinha.Append('Cart�o: ').Append(Pedido.numMesa);
          end
        else
          raise Exception.Create('Erro no tipo de pedido');
      end;

      informacoes.add(novaLinha.ToString);

      Frelatorio
        .addInformacaoImportante(TInformacaoImportante
          .New(titulo,10, 50, 11, informacoes));

      Informacoes.Clear();
      novaLinha.Clear;

      if FfechamentoMesa then
      begin
        Frelatorio
          .addInformacaoSimples(TInformacaoSimples
           .New('*FECHAMENTO DE MESA','', 9, 50));
      end;

      if not Pedido.dataHora.isEmpty then
      begin
        novaLinha.Append('Data-Hora: ').Append(Pedido.dataHora);

        Frelatorio
          .addInformacaoSimples(TInformacaoSimples
           .New(novaLinha.ToString,'', 9, 38));
      end;


      case FtipoPedido of
        1: FormataPedidoEntrega();
        2: FormataPedidoBalcao();
        3: FormataPedidoMesa();
        4: FormataPedidoCartao();
      end;

    end;

  finally
    informacoes.free;
    novaLinha.free;
  end;

end;

procedure TModelCupom.dadosProduto;
var
  item: TModelItens;
  i, aux, qtdCharQuantidade: integer;
  Produto: TModelProduto;
  colunas: TList<string>;
  qtdChar: TList<integer>;
  tiposProdutos: TList<string>;
  linha, categoria: string;
  linhaProduto: TStringBuilder;
  ItemExtra: TModelItemExtra;
  charsCod, charsDescricao, charsQtd, charsValor: integer;
begin
  colunas := TList<string>.Create;
  tiposProdutos := TList<string>.Create;
  qtdChar := TList<integer>.Create;
  linhaProduto := TStringBuilder.Create;

  if FGenerica then
  begin
    charsCod:=3;
    charsDescricao:=28;
    charsQtd:=3;
    charsValor:=7;
  end
  else
  begin
    charsCod:=3;
    charsDescricao:=13;
    charsQtd:=3;
    charsValor:=7;
  end;

  try
    aux := 0;
    if length(Itens) > 0 then
    begin
      for item in Itens do
      begin
        item.qtd:=item.qtd+'x';
        if tiposProdutos.count = 0 then
        begin
          // adiciono a categoria se n�o existir nenhuma categoria
          categoria := item.categoria;
          categoria := '--' + categoria;
          categoria := categoria.PadRight(48, '-');
          Frelatorio.addInformacaoSimples(TInformacaoSimples.New(categoria,
            '', 9, 100));

          tiposProdutos.add(item.categoria);
        end
        else
        begin
          // verifico se j� este a categoria do produto
          categoria := '';
          for linha in tiposProdutos do
          begin
            if linha = item.categoria then
              break
            else
              categoria := item.categoria;
          end;

          if not categoria.isEmpty then
          begin
            categoria := '--' + categoria;
            categoria := categoria.PadRight(38, '-');

            Frelatorio.addInformacaoSimples
              (TInformacaoSimples.New(categoria, '', 9, 38));

            tiposProdutos.add(item.categoria);
          end;
        end;

        for Produto in item.produtos do
        begin

          if Produto = item.produtos[0] then
          begin
            qtdCharQuantidade := 6;

            if FimpCod then
            begin
              if not item.tamanho.isEmpty then
              begin
                colunas.add(item.codProduto);
                qtdChar.add(charsCod);
                colunas.add(item.tamanho);
                qtdChar.add(length(item.tamanho));

                Frelatorio.addInformacoesLista
                  (TInformacaoLista.New(colunas, qtdChar));

                colunas.Clear();
                qtdChar.Clear();
                colunas.add('');
                qtdChar.add(charsCod);
              end
              else
              begin
                colunas.add(item.codProduto);
                qtdChar.add(charsCod);
              end;
            end
            else
            begin
              if not item.tamanho.isEmpty then
              begin
                colunas.add(item.tamanho);
                qtdChar.add(length(item.tamanho));
                Frelatorio.addInformacoesLista
                  (TInformacaoLista.New(colunas, qtdChar));

                colunas.Clear();
                qtdChar.Clear();
              end;

              aux := charsCod;
            end;

            if not FImpValor then
              aux := aux + charsValor;

            linhaProduto.Append(Produto.descricao.trim);
            if length(Produto.ItemExtra) > 0 then
            begin
              linhaProduto.Append('(');
              for ItemExtra in Produto.ItemExtra do
              begin
                if ItemExtra = Produto.ItemExtra[high(Produto.ItemExtra)] then
                  linhaProduto.Append(ItemExtra.descricao)
                else
                  linhaProduto.Append(ItemExtra.descricao).Append(',');
              end;
              linhaProduto.Append(')');
            end;

            qtdCharQuantidade := qtdCharQuantidade - length(item.qtd);
            if qtdCharQuantidade < 0 then
              qtdCharQuantidade := 0;

            colunas.add(linhaProduto.ToString);
            qtdChar.add(aux + qtdCharQuantidade + charsDescricao);

            colunas.add(item.qtd);
            qtdChar.add(length(item.qtd));

            if FImpValor then
            begin
              colunas.add(item.valor);
              qtdChar.add(charsValor);
            end;

            Frelatorio.addInformacoesLista(TInformacaoLista.New(colunas,
              qtdChar));

            linhaProduto.Clear;
            colunas.Clear;
            qtdChar.Clear;
          end
          else
          begin
            qtdCharQuantidade := 6;
            aux := 0;
            if FimpCod then
            begin
              colunas.add('');
              qtdChar.add(charsCod);
            end
            else
              aux := charsCod;

            if not FImpValor then
              aux := aux + charsValor;

            linhaProduto.Append(Produto.descricao);
            if length(Produto.ItemExtra) > 0 then
            begin
              linhaProduto.Append('(');
              for ItemExtra in Produto.ItemExtra do
              begin
                if ItemExtra = Produto.ItemExtra[high(Produto.ItemExtra)] then
                  linhaProduto.Append(ItemExtra.descricao)
                else
                  linhaProduto.Append(ItemExtra.descricao).Append(',');
              end;
              linhaProduto.Append(')');
            end;

            qtdCharQuantidade := qtdCharQuantidade - length(item.qtd);
            if qtdCharQuantidade < 0 then
              qtdCharQuantidade := 0;

            colunas.add(linhaProduto.ToString);
            qtdChar.add(aux + qtdCharQuantidade + charsDescricao);

            colunas.add('');
            qtdChar.add(length(item.qtd));

            if FImpValor then
            begin
              colunas.add('');
              qtdChar.add(charsValor);
            end;

            Frelatorio.addInformacoesLista(TInformacaoLista.New(colunas,
              qtdChar));
            linhaProduto.Clear;
            colunas.Clear;
            qtdChar.Clear;
          end;
        end;
      end;
    end;
  finally
    linhaProduto.free;
    colunas.free;
    tiposProdutos.free;
    qtdChar.free;
  end;
end;

procedure TModelCupom.descontos;
begin
  if assigned(Pedido) then
    if (Pedido.ValorDesconto <> '0,00') and not(Pedido.ValorDesconto.isEmpty)
    then
    begin
      Frelatorio.addInformacaoSimples(TInformacaoSimples.New('Desconto:',
        'R$' + Pedido.ValorDesconto, 9, 28));
    end;
end;

destructor TModelCupom.Destroy;
var
  i: integer;
begin
  if assigned(Fpedido) then
    Fpedido.free;

  if assigned(Fcliente) then
    Fcliente.free;

  if length(Fitens) > 0 then
    for i := 0 to length(Fitens) - 1 do
      Fitens[i].free;

  if assigned(Festabelecimento) then
    Festabelecimento.free;

  inherited;
end;

procedure TModelCupom.dividirPor;
begin
  if assigned(Pedido) and (FfechamentoMesa) then
  begin
    Frelatorio.addInformacaoSimples(TInformacaoSimples.New('Dividir por ' +
      Pedido.dividirPor, 'R$' + Pedido.ValorDivisao, 9, 28));
  end;

end;

procedure TModelCupom.FormataPedidoBalcao;
begin
  dadosCliente();
end;

procedure TModelCupom.FormataPedidoCartao;
begin
  dadosCliente();
end;

procedure TModelCupom.FormataPedidoEntrega;
begin
  dadosCliente();
  dadosEntrega();
end;

procedure TModelCupom.FormataPedidoMesa;
begin
  dadosCliente();
end;

procedure TModelCupom.Imprimir(nomeComputador, nomeImpressora, usuario: string; margemEsquerda: integer);
begin
  Frelatorio:= TRelatorioTermica.create(margemEsquerda);

  Frelatorio.Ref;

  PrepararImpressao(nomeImpressora, nomeComputador, usuario);

  Frelatorio.imprimir(
  'Cupom n:' + Pedido.numPedido,
  nomeComputador,
  nomeImpressora);
end;

procedure TModelCupom.imprimir(nomeComputador, nomeImpressora, usuario: string);
begin
  Frelatorio:= TRelatorioMatricial.create();
  FGenerica:= True;
  Frelatorio.Ref;

  PrepararImpressao(nomeImpressora, nomeComputador, usuario);

  Frelatorio.imprimir(
  'Cupom n:' + Pedido.numPedido,
  nomeComputador,
  nomeImpressora);
end;

procedure TModelCupom.PrepararImpressao(impressora, computador, usuario: string);
var
  infRodaPe: TList<string>;
  linha: TStringBuilder;
begin
  infRodaPe := TList<string>.Create;
  linha:= TStringBuilder.create();
  try
    dadosEstabelecimento;
    dadosPedido;
    dadosLegProduto;
    dadosProduto;
    subTotal;
    descontos;
    acrescimos;
    taxaEntrega;
    total;
    troco;
    dividirPor;
    bandeiraCartao;
    obsPagamento;

    if not computador.IsEmpty then
      linha.append('\\'+ computador);

    if not impressora.IsEmpty then
      linha.append('\'+ impressora);

    if not computador.IsEmpty then
      linha.append('  Usu�rio: '+ usuario);

    infRodaPe.add(linha.ToString());
    infRodaPe.add('www.centralsiclop.com.br');
    Frelatorio.addInformacaoRodape(TInformacaoRodape.New(infRodaPe, 7, 70));
  finally
    linha.free;
    infRodaPe.free;
  end;
end;

procedure TModelCupom.obsPagamento;
var
  informacoes: TList<string>;
begin
  informacoes := TList<string>.Create();
  try
    if assigned(Pedido) then
      if not(Pedido.obsPagamento.isEmpty) then
      begin

        informacoes.add('Obs: '+Pedido.obsPagamento);

        Frelatorio.addInformacaoImportante(TInformacaoImportante.New('',
          12, 40, 7, informacoes));

      end;
  finally
    FreeAndNil(informacoes);
  end;

end;

procedure TModelCupom.preparaImpressao(tipoCupom: integer;
  segundaVia, impCod, impValor, fechamentoMesa: boolean);
begin
  FtipoPedido := tipoCupom;
  FsegundaVia := segundaVia;
  FimpCod := impCod;
  FImpValor := impValor;
  FfechamentoMesa := fechamentoMesa;

  if assigned(Pedido) then
  begin
    if Pedido.altPedido.isEmpty then
      Pedido.altPedido:= 'N';

    FAltPedido:= CharInSet(Pedido.altPedido[low(Pedido.altPedido)], ['s', 'S']);
  end;
end;

procedure TModelCupom.subTotal;
var
  linha: string;
begin
        linha := linha.PadRight(48, '-');

      Frelatorio.addInformacaoSimples
        (TInformacaoSimples.New(linha, '', 9, 100));


  if assigned(Pedido) then
    if (Pedido.valorTotal <> Pedido.valorParcial) and
      not(Pedido.valorParcial.isEmpty) then
    begin
      Frelatorio.addInformacaoSimples
        (TInformacaoSimples.New('Valor Parcial:',
        'R$' + Pedido.valorParcial, 9, 28));
    end;

end;

procedure TModelCupom.taxaEntrega;

begin
  if FtipoPedido = 1 then
  begin
    if assigned(Cliente) then
      if assigned(Cliente.Endereco) then
        if (Cliente.Endereco.taxaEntrega <> '0,00') and
          not(Cliente.Endereco.taxaEntrega.isEmpty) then
        begin
          Frelatorio.addInformacaoSimples
            (TInformacaoSimples.New('Taxa de Entrega:',
            'R$' + Cliente.Endereco.taxaEntrega, 9, 28));
        end;
  end
  else if (FtipoPedido in [3, 4]) and (FfechamentoMesa) then
  begin
    if assigned(Pedido) then
      if (Pedido.taxaServico <> '0,00') and not(Pedido.taxaServico.isEmpty) then
        Frelatorio.addInformacaoSimples
          (TInformacaoSimples.New('Taxa de Servi�o:',
          'R$' + Pedido.taxaServico, 9, 28));

  end;
end;

procedure TModelCupom.total;

begin
  if assigned(Pedido) then
    if (Pedido.valorTotal <> '0,00') and not(Pedido.valorTotal.isEmpty) then
    begin

      Frelatorio.addInformacaoSimples(TInformacaoSimples.New('Total:',
        'R$' + Pedido.valorTotal, 11, 28));
    end;

end;

procedure TModelCupom.troco;

begin
  if assigned(Pedido) then
    if (Pedido.troco <> '0,00') and not(Pedido.troco.isEmpty) then
    begin
      Frelatorio.addInformacaoSimples(TInformacaoSimples.New('Troco:',
        'R$' + Pedido.troco, 11, 28));
    end;
end;

end.
