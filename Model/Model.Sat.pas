unit Model.Sat;

interface

const
  PASTACUPOM = 'C:\SICLOP\CUPOM\';

type
  TInformacoesSAT = class
  private
    Ftxt: TArray<string>;
  public
    property txt: TArray<string> read Ftxt write Ftxt;
    procedure SalvaTXT;
  end;

implementation

uses
  System.SysUtils, Model.Txt;

{ TInformacoesSAT }

procedure TInformacoesSAT.SalvaTXT;
var
  nomeDoArquivo: TStringBuilder;
  conteudoTxt: TStringBuilder;
  item: string;
begin
  conteudoTxt := TStringBuilder.Create;
  nomeDoArquivo := TStringBuilder.Create;
  try
    nomeDoArquivo.Append(FormatDateTime('ddmmyyyyhhnnss', Now)).Append('.SCP');

    for item in txt do
    begin
      conteudoTxt.Append(item).AppendLine();
    end;

    TModelTxt.GravarTxt(nomeDoArquivo.ToString, PASTACUPOM, conteudoTxt.ToString);
  finally
    nomeDoArquivo.Free;
    conteudoTxt.Free;
  end;

end;

end.
