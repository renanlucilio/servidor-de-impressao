unit Model.Itens;

interface

uses Model.Produto;

type
  TModelItens = class
  private
    FcodProduto: string;
    Fcategoria: string;
    Fqtd: string;
    Fvalor: string;
    Fprodutos: TArray<TModelProduto>;
    Ftamanho: string;
  public
    constructor Create;
    destructor Destroy; override;
    property codProduto: string read FcodProduto write FcodProduto;
    property categoria: string read Fcategoria write Fcategoria;
    property qtd: string read Fqtd write Fqtd;
    property valor: string read Fvalor write Fvalor;
    property tamanho: string read Ftamanho write Ftamanho;
    property produtos: TArray<TModelProduto> read Fprodutos write Fprodutos;
  end;

implementation

{ TModelItens }

constructor TModelItens.Create;
begin
  inherited Create;
end;

destructor TModelItens.Destroy;
var
  I: integer;
begin
  if length(Fprodutos) > 0 then
    for I := 0 to length(Fprodutos) - 1 do
      Fprodutos[I].free;
  inherited;
end;

end.
