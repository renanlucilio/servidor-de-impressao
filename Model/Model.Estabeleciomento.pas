unit Model.Estabeleciomento;

interface

uses Model.Endereco, Model.Contato;

type
   TModelEstabelecimento = class
   private
      FnomeFantasia: string;
      Fendereco: TModelEndereco;
      Fcontatos: TArray<TModelContato>;
    Fcnpj: string;
   public
      constructor Create;
      destructor Destroy; override;
      property nomeFantasia: string read FnomeFantasia write FnomeFantasia;
      property Endereco: TModelEndereco read Fendereco write Fendereco;
      property contatos: TArray<TModelContato> read Fcontatos write Fcontatos;
      property cnpj: string read Fcnpj write Fcnpj;
   end;

implementation

{ TModelEstabelecimento }

constructor TModelEstabelecimento.Create;
begin
   inherited Create;
end;

destructor TModelEstabelecimento.Destroy;
var
   I: Integer;
begin
   if length(Fcontatos) > 0 then
      for I := 0 to length(Fcontatos) - 1 do
         Fcontatos[I].free;

   if assigned(Fendereco) then
      Fendereco.free;

   inherited;
end;

end.
