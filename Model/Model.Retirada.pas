unit Model.Retirada;

interface

uses
  uRelatorio.Interfaces,
  System.SysUtils,
  Model.Estabeleciomento;

type
   TModelRetirada = class
   private
      Frelatorio: IRelatorio;
      Festabelecimento: TModelEstabelecimento;
      Fresponsavel: string;
      Fdestinatario: string;
      Fmotivo: string;
      Fvalor: string;
      Fdata: string;
      procedure dadosEstabelecimento;
      procedure dadosRetirada;
      procedure valorRetirada;
      procedure motivoRetirada;
      procedure PrepararImpressao(impressora, computador, usuario: string);
   public
      Constructor Create;
      Destructor Destroy; override;
      property estabelecimento: TModelEstabelecimento read Festabelecimento
        write Festabelecimento;
      property responsavel: string read Fresponsavel write Fresponsavel;
      property destinatario: string read Fdestinatario write Fdestinatario;
      property motivo: string read Fmotivo write Fmotivo;
      property valor: string read Fvalor write Fvalor;
      property data: string read Fdata write Fdata;
      procedure Imprimir(nomeComputador, nomeImpressora, usuario: string; margemEsquerda: integer); overload;
      procedure imprimir(nomeComputador, nomeImpressora, usuario: string); overload;
   end;

implementation

{ TModelRetirada }

uses
   uRelatorio.InformacacaoImportante,
   uRelatorio.InformacaoRodape,
   uRelatorio.InformativosSimples,
   System.Generics.Collections,
   Model.Contato,
   uRelatorio.Matricial, uRelatorio.Termica;

constructor TModelRetirada.Create;
begin
   inherited;
   Festabelecimento := TModelEstabelecimento.Create;
end;

procedure TModelRetirada.dadosEstabelecimento;
var
   titulo: string;
   informacoes: TList<string>;
   novaLinha: TStringBuilder;
   item: TModelContato;
begin
   informacoes := TList<string>.Create;
   novaLinha := TStringBuilder.Create;
   try
      if Assigned(estabelecimento) then
      begin
         titulo := estabelecimento.nomeFantasia;

         if Assigned(estabelecimento.endereco) then
         begin
            novaLinha.Append('Endere�o: ');

            if not estabelecimento.endereco.logradouro.IsEmpty then
            begin
               novaLinha.Append(estabelecimento.endereco.logradouro);

               if not estabelecimento.endereco.numero.IsEmpty then
                  novaLinha.Append(', ')
                    .Append(estabelecimento.endereco.numero);

               if not estabelecimento.endereco.complemento.IsEmpty then
                  novaLinha.Append(' - ')
                    .Append(estabelecimento.endereco.complemento);

               if not estabelecimento.endereco.bairro.IsEmpty then
                  novaLinha.Append(' - ')
                    .Append(estabelecimento.endereco.bairro);

               if not estabelecimento.endereco.cidade.IsEmpty then
                  novaLinha.Append(' - ')
                    .Append(estabelecimento.endereco.cidade);

               if not estabelecimento.endereco.uf.IsEmpty then
                  novaLinha.Append(' - ').Append(estabelecimento.endereco.uf);

               if not estabelecimento.endereco.cep.IsEmpty then
                  novaLinha.Append(' - ').Append(estabelecimento.endereco.cep);

               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;

            if not estabelecimento.endereco.complementoRua.IsEmpty then
            begin
               novaLinha.Append('Complemento Rua: ')
                 .Append(estabelecimento.endereco.complementoRua);
               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;

            if not estabelecimento.endereco.pontoReferencia.IsEmpty then
            begin
               novaLinha.Append('Ponto de referencia: ')
                 .Append(estabelecimento.endereco.pontoReferencia);
               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;

         end;

         if Assigned(estabelecimento.contatos) then
         begin
            for item in estabelecimento.contatos do
            begin
               if not item.tipo.IsEmpty then
                  novaLinha.Append(item.tipo).Append(': ');

               if not item.Contato.IsEmpty then
                  novaLinha.Append(item.Contato);

               if CharInSet(item.whatsapp[low(item.whatsapp)], ['s', 'S']) then
                  novaLinha.Append(' *WHATSAPP');

               informacoes.add(novaLinha.ToString);
               novaLinha.Clear;
            end;
         end;

      end;

      Frelatorio.addInformacaoImportante(TInformacaoImportante.New(titulo,
        12, 52, 7, informacoes));

   finally

      novaLinha.Free;
      informacoes.Free;
   end;
end;

procedure TModelRetirada.dadosRetirada;
var
   titulo: string;
   informacoes: TList<string>;
   novaLinha: TStringBuilder;
begin

   informacoes := TList<string>.Create;
   novaLinha := TStringBuilder.Create;
   try
      titulo := 'Recibo de retirada';

      if not destinatario.IsEmpty then
      begin
         novaLinha.Append('Destinatario: ').Append(destinatario);
         informacoes.add(novaLinha.ToString);
         novaLinha.Clear;
      end;

      if not responsavel.IsEmpty then
      begin
         novaLinha.Append('Responsavel: ').Append(responsavel);
         informacoes.add(novaLinha.ToString);
         novaLinha.Clear;
      end;

      if not data.IsEmpty then
      begin
         novaLinha.Append('Data/Hora: ').Append(data);
         informacoes.add(novaLinha.ToString);
         novaLinha.Clear;
      end;

      Frelatorio.addInformacaoImportante(TInformacaoImportante.New(titulo,
        12, 52, 7, informacoes));
   finally
      novaLinha.Free;
      informacoes.Free;
   end;

end;

destructor TModelRetirada.Destroy;
begin
   if Assigned(Festabelecimento) then
      Festabelecimento.Free;

   inherited;

end;

procedure TModelRetirada.Imprimir(nomeComputador, nomeImpressora, usuario: string; margemEsquerda: integer);
begin
  Frelatorio:= TRelatorioTermica.create(margemEsquerda);

  Frelatorio.Ref;

  PrepararImpressao(nomeImpressora, nomeComputador, usuario);

   Frelatorio.imprimir(
    'Recibo de retirada: ' + data,
    nomeComputador,
    nomeImpressora);
end;

procedure TModelRetirada.imprimir(nomeComputador, nomeImpressora, usuario: string);
begin
    Frelatorio := TRelatorioMatricial.Create();

  Frelatorio.Ref;

  PrepararImpressao(nomeImpressora, nomeComputador, usuario);

   Frelatorio.imprimir(
    'Recibo de retirada: ' + data,
    nomeComputador,
    nomeImpressora);
end;

procedure TModelRetirada.PrepararImpressao(impressora, computador, usuario: string);
var
  infRodaPe: TList<string>;
  linha: TStringBuilder;
begin

  infRodaPe := TList<string>.Create;
  linha:= TStringBuilder.Create();
  try
    dadosEstabelecimento;
    dadosRetirada;
    valorRetirada;
    motivoRetirada;

    if not computador.IsEmpty then
      linha.append('\\'+ computador);

    if not impressora.IsEmpty then
      linha.append('\'+ impressora);

    if not computador.IsEmpty then
      linha.append('  Usu�rio: '+ usuario);

    infRodaPe.add(linha.ToString());

    infRodaPe.add('www.centralsiclop.com.br');
    Frelatorio.addInformacaoRodape(TInformacaoRodape.New(infRodaPe, 7, 70));
  finally
    linha.Free;
    infRodaPe.Free;
  end;
end;

procedure TModelRetirada.motivoRetirada;
var
   titulo: string;
   informacoes: TList<string>;
begin
   informacoes := TList<string>.Create;
   try
      titulo := 'Motivo';
      if not(motivo.IsEmpty) then
         informacoes.add(motivo);

      informacoes.add('|');
      informacoes.add('________________________________________');

      if not destinatario.IsEmpty then
         informacoes.add(destinatario);

      Frelatorio.addInformacaoImportante(TInformacaoImportante.New(titulo,
        12, 52, 7, informacoes));
   finally
      informacoes.Free;
   end;
end;

procedure TModelRetirada.valorRetirada;
var
   titulo: string;
   informativo: string;
begin
   titulo := 'valor:';
   if not valor.IsEmpty then
      informativo := 'R$' + valor;

   Frelatorio.addInformacaoSimples(TInformacaoSimples.New(titulo,
     informativo, 10, 30));
end;

end.
