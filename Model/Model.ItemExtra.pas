unit Model.ItemExtra;

interface

type
   TModelItemExtra = class
   private
      Fdescricao: string;
      Fimportante: string;
   public
      constructor Create;
      destructor Destroy; override;
      property descricao: string read Fdescricao write Fdescricao;
      property importante: string read Fimportante write Fimportante;
   end;

implementation

{ TModelItemExtra }

constructor TModelItemExtra.Create;
begin
   inherited Create;
end;

destructor TModelItemExtra.Destroy;
begin

   inherited;
end;

end.
