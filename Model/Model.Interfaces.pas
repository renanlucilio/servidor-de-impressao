unit Model.Interfaces;

interface

uses Model.Cupom, Model.Retirada, Model.Extrato, Model.Impressao;

type

   iModelServidor = interface
      ['{AEFA23BD-41AB-43AE-9FB9-F05AF593C3BB}']
      function GetcodigoServidor: string;
      function getLogado: boolean;
      function gerarCodigoServidor: iModelServidor;
      function login(login, senha: string): iModelServidor;
      function ligarServidor(aPerguntar: Boolean = true): iModelServidor;
      function iniciaComWindows: iModelServidor;
      function getConectado: boolean;
      property codigoServidor: string read GetcodigoServidor;
      property logado: boolean read getLogado;
      property conectado: boolean read getConectado;
   end;

   TarListaImpressoes = Tarray<TModelImpressoes>;

   iModelListaImpressoes = interface
      ['{78211E86-5F86-4C92-AAF5-F65E45EF3B14}']
      function getListaImpressoes: TarListaImpressoes;
      procedure setListaImpressoes(const Value: TarListaImpressoes);
      property listaImpressoes: TarListaImpressoes read getListaImpressoes
        write setListaImpressoes;
   end;

implementation

end.
