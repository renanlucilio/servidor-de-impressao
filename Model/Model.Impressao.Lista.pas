unit Model.Impressao.Lista;

interface

uses Model.Interfaces;

type
   TModelListaImpressoes = class(TInterfacedObject, iModelListaImpressoes)
   private
      FlistaImpressoes: TarListaImpressoes;
      function getListaImpressoes: TarListaImpressoes;
      procedure setListaImpressoes(const Value: TarListaImpressoes);
   public
      Destructor Destroy; override;
      property listaImpressoes: TarListaImpressoes read getListaImpressoes
        write setListaImpressoes;
   end;

implementation

{ TModelListaImpressoes }

destructor TModelListaImpressoes.Destroy;
var
   i: integer;
begin
   if Length(FlistaImpressoes) > 0 then
      for i := 0 to Length(FlistaImpressoes) - 1 do
         FlistaImpressoes[i].Free;
   inherited;
end;

function TModelListaImpressoes.getListaImpressoes: TarListaImpressoes;
begin
   result := FlistaImpressoes;
end;

procedure TModelListaImpressoes.setListaImpressoes
  (const Value: TarListaImpressoes);
begin
   FlistaImpressoes := Value;
end;

end.
