unit Model.Cliente;


interface

uses Model.Contato, Model.Endereco;

type
   TModelCliente = class
   private
      Fnome: string;
      Frestricao: string;
      Fcontato: TArray<TModelContato>;
      Fendereco: TModelEndereco;
      FultimoPedido: string;
   public
      constructor Create;
      destructor Destroy; override;
      //
      property nome: string read Fnome write Fnome;
      property restricao: string read Frestricao write Frestricao;
      property Contato: TArray<TModelContato> read Fcontato write Fcontato;
      property Endereco: TModelEndereco read Fendereco write Fendereco;
      property ultimoPedido: string read FultimoPedido write FultimoPedido;
   end;

implementation

{ TModelCliente }

constructor TModelCliente.Create;
begin
   inherited Create;
end;

destructor TModelCliente.Destroy;
var
   I: Integer;
begin
   if length(Fcontato) > 0 then
      for I := 0 to length(Fcontato) - 1 do
         Fcontato[I].free;

   if assigned(Fendereco) then
      Fendereco.free;

   inherited;
end;

end.

